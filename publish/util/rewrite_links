#!/bin/sh

# SPDX-FileCopyrightText: 2024 DorotaC
#
# SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later

set -e

# Files in the doc/ directory need to link "crates/" to sources from the forge context, while in the mdbook context they should link to docs whenever possible. Only a couple of locations can be unambiguously connected to docs, the rest should go to the source repo.

# I'M SORRY

# sed -i -E 's/\]\(  REGEX LINK  \/?\)/\](   TARGET   )/g' $1

# the "conv" directory contains the "crispyconv" crate
sed -i -E 's/\]\(\.\.\/crates\/conv\/?\)/\](public-api\/crispyconv)/g' $1
# link toplevel projects to their public API references - that seems to be what you want when browsing documentation
sed -i -E 's/\]\(\.\.\/crates\/dma-boom\/?\)/\](public-api\/dma-boom)/g' $1
sed -i -E 's/\]\(\.\.\/crates\/crispy\/?\)/\](public-api\/crispy)/g' $1
sed -i -E 's/\]\(\.\.\/crates\/vidi\/?\)/\](public-api\/vidi)/g' $1
sed -i -E 's/\]\(\.\.\/crates\/const_enum\/?\)/\](public-api\/const_enum)/g' $1
sed -i -E 's/\]\(\.\.\/crates\/media-subsystem\/?\)/\](public-api\/media_subsystem)/g' $1

# `cargo doc` puts binaries straight in the main directory
sed -i -E 's/\]\(\.\.\/crates\/.*\/bin\/(.*)\.rs\)/\](public-api\/\1)/g' $1

# let the rest link to the source browser
sed -i -E 's/\]\(\.\.\/crates\//\](https:\/\/codeberg\.org\/libobscura\/libobscura\/src\/branch\/master\/crates\//g' $1
