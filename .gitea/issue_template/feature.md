---
name: 'Feature'
about: 'Suggest something new.'
labels:
  - Kind/Feature
---

<!-- Write a general description comparing what is now and what you suggest -->

## Benefit to users

<!-- How will this make end users' experience better? What will application programmers gain? -->

## Benefit to manufacturers

<!-- How will this make device manufactuers more likely to use Linux for cameras? Think about new classes of supported features or devices. -->

## Benefit to libobscura

<!-- What new options does it open for the project? What other issues might this solve? -->

## Downsides

<!-- If implemented, will complexity go up? Will some options get closed? Etc. -->

## Tasks

<!-- Put a breakdown of tasks if you already have an idea. Or not. -->