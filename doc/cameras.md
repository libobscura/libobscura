# Adding cameras

The camera support API has not been the focus yet.

This is quickly changing, with [Librem 5 support](https://codeberg.org/libobscura/libobscura/issues/9) being an upcoming target.

## Status

For now, there is a pair of traits matching the needs of the UVC cameras: the minimal [UnacquiredCameraImpl](private-api/vidi/pipelines/trait.UnacquiredCameraImpl.html) and the actually interesting [AcquiredCameraImpl](private-api/vidi/pipelines/trait.AcquiredCameraImpl.html).

(Those traits are still in flux. After working on this for some time, it's not clear to me which parts should be generically implemented for every possible camera and which should call into the trait for specialized treatment. See module documentation.)

Every pipeline handler must place a [CheckFn](private-api/vidi/pipelines/type.CheckFn.html) in the [PIPELINES](private-api/vidi/pipelines/constant.PIPELINES.html) array. That function scans for cameras supported by your handler. Best see how [the UVC one](private-api/vidi/pipelines/uvc/struct.Checker.html#method.check_match) works.

## Limitations

Currently only V4L2-based cameras are even considered. So no IP cameras and no generated streams. Those may be better handled with something like PipeWire.

Even if there is another kind of cameras worthy supporting, we're focusing on this API first.

## Solver

Libobscura relies on a solver to configure the camera pipeline. The solver uses rules describing every device in the pipeline to find a path through the devices, where the output image satisfies user's constraints.

The rules are stored and described in the [devices.pl](../crates/vidi/config/devices.pl) file.

While the rules don't support arbitrary constraints yet (methematical operations are very limited), this works and proves that a rules-based view is useful to build pipelines.

### New cameras

To support a new camera pipeline, add rules describing every V4L2 entity in the pipeline: the sensor formats, the processing nodes, and the available conversions from Mbus to FourCC.

The solver is generic and will figure out the rest.

To make sure that your configuration checks out, query the device with the [obscura_configs](../crates/vidi-tools/src/bin) tool, like this:

```
LIBOBSCURA_DEVICES_DIR=crates/vidi/config cargo run --bin obscura_configs -- --repl 'uvcvideo:Integrated Camera: Integrated C:7fffe2fe3bab469b:Camera 1'
```

It will list the contents of the database and let you query it as the solver does.