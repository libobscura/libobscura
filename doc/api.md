# API reference

All libobscura crates with public API have their reference posted online:

- [vidi](https://libobscura.codeberg.page/private-api/vidi/)
- [crispy](https://libobscura.codeberg.page/private-api/crispy/)
- [v4l-stream](https://libobscura.codeberg.page/private-api/v4l/)
- [dma-boom](https://libobscura.codeberg.page/private-api/dma_boom/)
- [media-subsystem](https://libobscura.codeberg.page/private-api/media_subsystem/)
- [v4l2-subdev](https://libobscura.codeberg.page/private-api/v4l2_subdev/) 
- [logru-arithmeticl](https://libobscura.codeberg.page/private-api/logru_arithmetic/)
- [logru-repl](https://libobscura.codeberg.page/private-api/logru_repl/)
- [const_enum](https://libobscura.codeberg.page/private-api/const_enum/)
- [error-backtrace](https://libobscura.codeberg.page/private-api/error_backtrace/)