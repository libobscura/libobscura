# Summary

- [What is libobscura?](README.md)
- [Community](community.md)
- [Hacking](hacking.md)
    - [Making applications](applications.md)
    - [Crates API](api.md)
    - [Adding cameras](cameras.md)
    - [Internal API](reference.md)
- [Copying](COPYING.md)