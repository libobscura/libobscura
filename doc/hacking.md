# Hacking

## Installation

There are three main ways to hack with libobscura.

- [write applications](applications.md) using it,
- [add support for a new camera](cameras.md), and
- contribute to the internals.

Either way, you'll need to compile it, and for that, Rust is used. On Fedora:

```
sudo dnf -y install rust
```

### Librem 5

Ancient version ships with PureOS. It can be replaced with a new one from rustup:

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Test application

When modifying libobscura, it's best to start with a working test application.

For USB cameras, the end-to-end test is the "[demo.rs](../crates/vidi-examples/src/bin/demo.rs)".

Check out the code and build it:

```
git clone https://codeberg.org/libobscura/libobscura.git
cargo run --example demo
```

## Debug the library

Libobscura uses the [tracing] crate to present debug messages to the user.

Use your favorite debugging library to enable debug output. For [tracing-subscriber], add this line at the start of your program:

```
tracing_subscriber::fmt::init();
```

Builtin tools and examples generally do that. You can then enable tracing at runtime, for example:

```
RUST_LOG=debug cargo run --bin obscura_list
```

## Contribute to internals

The internals are documented throughout the code base, so refer to every file in the sources. Please [report a bug](https://codeberg.org/libobscura/libobscura/issues/new?template=.gitea%2fissue_template%2fbug.yaml) if anything that's missing holds your understanding.

There's an [old design document](../crates/vidi/doc/design.md) in libvidi describing the general goals of the architecture. It's a little outdated and stream-of-thought-like, but maybe it helps you understand the motivations.
