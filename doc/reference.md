# Internal reference

The APIs needed to add support for a new camera pipeline is in the [device config file](../crates/vidi/config/devices.pl).

Likewise, the APIs to add new processing shaders to *crispy* are [not public](private-api/crispy/index.html).

The private API reference documents all of that, and more, for every libobscura crate:

- [vidi](https://libobscura.codeberg.page/private-api/vidi/)
- [crispy](https://libobscura.codeberg.page/private-api/crispy/)
- [v4l-stream](https://libobscura.codeberg.page/private-api/v4l/)
- [dma-boom](https://libobscura.codeberg.page/private-api/dma_boom/)
- [media-subsystem](https://libobscura.codeberg.page/private-api/media_subsystem/)
- [v4l2-subdev](https://libobscura.codeberg.page/private-api/v4l2_subdev/) 
- [logru-arithmeticl](https://libobscura.codeberg.page/private-api/logru_arithmetic/)
- [logru-repl](https://libobscura.codeberg.page/private-api/logru_repl/)
- [const_enum](https://libobscura.codeberg.page/private-api/const_enum/)
- [error-backtrace](https://libobscura.codeberg.page/private-api/error_backtrace/)