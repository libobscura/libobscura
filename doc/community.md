# Community

A tool is only as good as it is useful. So please reach out to us with feedback and improvements.

Libobscura can't survive with one person behind it, so please contribute. Even if to say how you're using it.

## Contact

Libobscura has a [Matrix channel](https://matrix.to/#/#libobscura:chatwave.org).

You can ask a question [on the issue tracker](https://codeberg.org/libobscura/libobscura/issues/new?template=.gitea%2fissue_template%2fquestion.md).

Also, there is [a wiki](https://codeberg.org/libobscura/libobscura/wiki) where users like you can write down their experiences and solutions – or read those of others.

Finally, you can contact one of the maintainers:

- dorotac [on Mastodon](https://fosstodon.org/@dcz) - main designer (**full time**)

## Contributing

Software is not just code, so there are many ways to make a difference:

- give the developers a high five
- improve this documentation
- [report bad documentation](https://codeberg.org/libobscura/libobscura/issues/new?template=.gitea%2fissue_template%2fbug.yaml)
- make your needs known to the developers via chat or [issue tracker](https://codeberg.org/libobscura/libobscura/issues/new?template=.gitea%2fissue_template%2ffeature.md)
- report your own experiences on the [wiki](https://codeberg.org/libobscura/libobscura/wiki)
- review [issues](https://codeberg.org/libobscura/libobscura/issues) and [contributions](https://codeberg.org/libobscura/libobscura/pulls)

By contributing code to libobscura, you agree to release it under the combination of licenses: LGPL 2.1 or later, or MPL 2.0. For changes to files in vidi-examples, you agree to release them under MIT or Apache-2.0 instead.

Don't be a jerk.

## Maintainers' duties

The maintainers will do their best to respond to code contributions and issues within a couple days.

When reviewing a contribution of code, the maintainer will clearly say which complaints need to be addressed before the contribution is accepted, and which ones don't.

## Development

As long as there is a maintainer in the above list, patches will get reviewed and issues triaged, both within a couple days. Security problems will likewise get fixed.

Adding new features will progress while the Prototype Fund funding is running (March 2025), and after that it will only happen randomly.

If you'd like to see this experiment continue at full power, you have the following options:
- contribute code or test your hardware or offer your camera knowledge to Dorota,
- become a maintainer (talk to Dorota),
- offer Dorota a wad of money, or
- offer Dorota help with other projects (Wayland input methods) to free up the time to work on this.
