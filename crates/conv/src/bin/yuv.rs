/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Loads raw YUYV data and converts it to RGB */

use clap::{Parser, ValueEnum};
use crispy::egl::DmabufImage;
use crispy::egl::headless;
use crispy::glium;
use crispy::yuv;
use crispyconv::{draw, get_bgra_tiff_writer, parse_quirks, read_format, ImageInfo, InputInfo, OutInfo};
use crispyconv::processing::render_yuv;
use std::path::PathBuf;
use tracing_subscriber;


#[derive(Clone, ValueEnum, Debug)]
#[clap(rename_all="verbatim")]
enum PixFormat {
    YUYV,
}

#[derive(Parser, Debug)]
#[clap(about)]
struct Args {
    /// Path to Bayer data
    input: PathBuf,
    /// Interpret file as raw data, assume those output parameters.
    ///
    /// Format: width_px,height_px,bpp
    #[clap(long = "raw", value_parser = OutInfo::parse_str)]
    format: Option<OutInfo>,
    /// Subpixel order
    #[clap(value_enum)]
    order: PixFormat,
    /// TIFF output
    output: PathBuf,
    /// Overrides the default set of hardware peculiarities.
    /// Defaults to most compatible.
    #[clap(long="quirks")]
    quirks: Option<String>,
}

fn main() {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let (info, reader) = read_format(args.format, args.input.as_path()).unwrap();
    let quirks = parse_quirks(args.quirks.as_deref()).unwrap();
    let out_info = match info {
        ImageInfo::Output(out) => out,
        ImageInfo::Input(InputInfo {dimensions:(w, h), bpp}) => OutInfo {
            width_px: w / 2,
            height_px: h,
            bpp
        },
    };
    let OutInfo { width_px, height_px, bpp } = out_info;
    let dimensions = (width_px, height_px);
    
    draw(
        headless::ContextRef::new().unwrap(),
        reader,
        bpp,
        quirks,
        (width_px * 2, height_px),
        dimensions,
        |egl: &headless::ContextRef, source: &DmabufImage, target: &mut glium::framebuffer::SimpleFrameBuffer, gpubuf_bpp, quirks, dimensions| render_yuv(
            egl,
            source,
            target,
            gpubuf_bpp,
            quirks,
            dimensions,
            yuv::ColorSpace::BT709,
        ),
        &mut get_bgra_tiff_writer(args.output.to_path_buf(), dimensions)
    ).unwrap();
}