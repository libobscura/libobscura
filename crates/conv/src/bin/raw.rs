/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Bayer raw processor */


use clap::Parser;
use crispy::egl::DmabufImage;
use crispy::egl::headless;
use crispy::glium;
use crispyconv::{draw, get_bgra_tiff_writer, read_format, parse_quirks, OutInfo, PixOrder, ImageInfo, InputInfo};
use crispyconv::processing::render_bayer;
use std::path::PathBuf;
use tracing_subscriber;

/// Processes Bayer data into RGB
#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Path to Bayer data
    input: PathBuf,
    /// Interpret file as raw data.
    /// Format: width_px,height_px,bpp
    #[clap(long = "raw", value_parser = OutInfo::parse_str)]
    format: Option<OutInfo>,
    /// Subpixel order
    #[clap(value_enum)]
    order: PixOrder,
    /// TIFF output
    output: PathBuf,
    /// Overrides the default set of hardware peculiarities.
    /// Defaults to most compatible.
    #[clap(long="quirks")]
    quirks: Option<String>,
}

fn main() {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let (info, reader) = read_format(args.format, args.input.as_path()).unwrap();
    let out_info = match info {
        ImageInfo::Output(out) => out,
        ImageInfo::Input(InputInfo {dimensions:(width_px, height_px), bpp}) => OutInfo { width_px, height_px, bpp},
    };
    let quirks = parse_quirks(args.quirks.as_deref()).unwrap();
    let OutInfo { width_px, height_px, bpp } = out_info;
    let dimensions = (width_px, height_px);
    
    draw(
        headless::ContextRef::new().unwrap(),
        reader,
        bpp,
        quirks,
        dimensions,
        dimensions,
        |egl: &headless::ContextRef, source: &DmabufImage, target: &mut glium::framebuffer::SimpleFrameBuffer, gpubuf_bpp, quirks, dimensions | render_bayer(egl, source, target, gpubuf_bpp, quirks, dimensions, args.order),
        &mut get_bgra_tiff_writer(args.output.to_path_buf(), dimensions)
    ).unwrap();
}