/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

use clap::Parser;
use crispyconv;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use tracing_subscriber;


fn main() {
    tracing_subscriber::fmt::init();
    argb_main().unwrap()
}

/// Processes BGRA data into RGB
#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Path to raw BGRA data
    input: PathBuf,
    /// Format: width_px,height_px,bpp
    #[clap(value_parser = crispyconv::RawInfo::parse_str)]
    format: crispyconv::RawInfo,
    /// TIFF output
    output: PathBuf,
}

fn argb_main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();
    
    // No reason to load it all frst, other than "it works now".
    let mut src = File::open(args.input)?;
    let mut b = Vec::new();
    src.read_to_end(&mut b)?;
    let mut b = b.chunks(4);
    crispyconv::write_bgra_pixels_to_tiff(
        &mut || b.next(),
        (args.format.width_px, args.format.height_px),
        &args.output,
    )?;
    Ok(())
}