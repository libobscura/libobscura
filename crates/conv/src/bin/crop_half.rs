/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Debug program: loads a file and then outputs its left side again */

use clap::{Parser, ValueEnum};
use crispy::egl::DmabufImage;
use crispy::egl::headless;
use crispy::error_backtrace;
use crispy::glium;
use crispy::shaders::crop::Crop;
use crispyconv::{draw, get_bgra_tiff_writer, read_format, parse_quirks, Bits, OutInfo, InputInfo, ImageInfo };
use crispyconv::quirks::Quirks;
use error_backtrace::{GenericError, Result};
use std::path::PathBuf;
use tracing_subscriber;


#[derive(Clone, ValueEnum)]
#[clap(rename_all="verbatim")]
enum PixFormat {
    /// Normal RGB+alpha image
    RGBA,
    /// 0-255 grayscale
    U8,
}

#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Path to Bayer data
    input: PathBuf,
    /// Interpret file as raw data.
    /// Format: width_px,height_px,bpp
    #[clap(long = "raw", value_parser = OutInfo::parse_str)]
    format: Option<OutInfo>,
    /// Subpixel order
    #[clap(value_enum)]
    order: PixFormat,
    /// TIFF output
    output: PathBuf,
    /// Overrides the default set of hardware peculiarities.
    /// Defaults to most compatible.
    #[clap(long="quirks")]
    quirks: Option<String>,
}


pub fn render_crop(
    egl: &headless::ContextRef,
    source: &DmabufImage,
    target: &mut impl glium::Surface,
    _gpubuf_bpp: Bits,
    _quirks: Quirks,
    dimensions: (u32, u32),
) -> Result<(), GenericError> {
    let facade = crispy::Facade::new(egl.clone(), dimensions);
    let shader = Crop::new(&facade, dimensions).map_err(GenericError)?;
    shader.convert(
        &facade,
        source.get_texture(),
        target,
        // TODO: carry the backtrace all the way up
    )
}

fn main() {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let (info, reader) = read_format(args.format, args.input.as_path()).unwrap();
    let out_info = match info {
        ImageInfo::Output(out) => out,
        ImageInfo::Input(InputInfo {dimensions:(width_px, height_px), bpp}) => OutInfo { width_px: width_px / 2, height_px, bpp},
    };
    let quirks = parse_quirks(args.quirks.as_deref()).unwrap();
    let OutInfo { width_px, height_px, bpp } = out_info;
    let dimensions = (width_px, height_px);
    
    draw(
        headless::ContextRef::new().unwrap(),
        reader,
        bpp,
        quirks,
        (width_px * 2, height_px),
        dimensions,
        |egl: &headless::ContextRef, source: &DmabufImage, target: &mut glium::framebuffer::SimpleFrameBuffer, gpubuf_bpp, quirks, dimensions| render_crop(egl, source, target, gpubuf_bpp, quirks, dimensions),
        &mut get_bgra_tiff_writer(args.output.to_path_buf(), dimensions)
    ).unwrap();
}