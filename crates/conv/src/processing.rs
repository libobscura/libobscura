/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Processing algorithms in re-useable form */

use crate::{PixOrder, Bits};
use crate::quirks;
use crate::quirks::Quirks;
use crispy::bayer::DebayerHq;
use crispy::cgmath::{Matrix3, Vector3, SquareMatrix};
use crispy::egl::DmabufImage;
use crispy::egl::headless::ContextRef;
use crispy::error_backtrace;
use crispy::glium;
use crispy::yuv;
use error_backtrace::{GenericError, Result};

pub fn render_bayer(
    egl: &ContextRef,
    source: &DmabufImage,
    target: &mut impl glium::Surface,
    gpubuf_bpp: Bits,
    quirks: Quirks,
    dimensions: (u32, u32),
    order: PixOrder,
) -> Result<(), GenericError> {
    // Appropriate for the L5 cameras
    let color_matrix = <&Matrix3::<_>>::from(&[
        1.0447192f32, -0.3827290, -0.1203956,
        -0.2059623, 1.3225799, 0.2009371,
        0.0837337, 0.1372769, 0.9852679,
    ]);
    let xyz_to_srgb = <&Matrix3::<_>>::from(&[
        3.2404542f32, -1.5371385, -0.4985314,
        -0.9692660,  1.8760108,  0.0415560,
        0.0556434, -0.2040259,  1.0572252,
    ]);
    let illuminants = [[1.0f32, 1.0, 1.0000000]];
    // TODO: more illuminants
    let wb = Matrix3::from_diagonal(Vector3::from(illuminants[0]));
    let balanced = wb * color_matrix;
    let color_matrix = balanced.invert().unwrap() * xyz_to_srgb;

    let shader_create = match (gpubuf_bpp, quirks.multi_byte_handling) {
        (Bits::B8, _)
        | (_, quirks::MultiByteHandling::R16) => DebayerHq::new_raw,
        (_, quirks::MultiByteHandling::R16AsG8R8) => DebayerHq::new_raw_rg88,
    };

    let egl_facade = crispy::Facade::new(egl.clone(), (640, 480));
    
    let shader = shader_create(&egl_facade, dimensions).map_err(GenericError)?;
    //let texture = crispy::shaders::import_target_texture(&shader.facade, &target.get_texture());
    //let mut target = crispy::shaders::texture_to_surface(&shader.facade, &texture)?;
    
    Ok(
        shader.convert(
            &egl_facade,
            source.get_texture(),
            target,
            order.into(),
            // Normalize output data to 1.0. I hope this works correctly.
            // The alternative is normalizing Bayer values by multiplication,
            // which is a hassle to write fast, compared to this constant here,
            // by which we must multiply anyway.
            color_matrix * match gpubuf_bpp {
                Bits::B8 => 1.0,
                Bits::B10 => 64.0,
                Bits::B16 => 1.0,
            },
        )
        .map_err(GenericError)?
    )
}

pub fn render_yuv(
    egl: &ContextRef,
    source: &DmabufImage,
    target: &mut impl glium::Surface,
    _gpubuf_bpp: Bits,
    _quirks: Quirks,
    dimensions: (u32, u32),
    colorspace: yuv::ColorSpace,
) -> Result<(), GenericError> {
    let facade = crispy::Facade::new(egl.clone(), dimensions);
    let shader = yuv::YuyvToRgba::new(&facade, dimensions)
        .map_err(GenericError)?;
    shader.convert(
        &facade,
        source.get_texture(),
        target,
        colorspace,
        yuv::Gamma::Identity,
        yuv::Letterboxing::No,
    )
}
