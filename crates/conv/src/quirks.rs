/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

use std::fmt;

macro_rules! ParseableEnum {
    (
        $(#[$a:meta])*
        pub enum $name:ident {
            $(
                $(#[$attr:meta])*
                $variant_name:ident,
            )*
        }
    ) => {
        $(#[$a])*
        pub enum $name {
            $(
                $(#[$attr])*
                $variant_name,
            )*
        }
        
        impl $name {
            fn parse(value: &str) -> Result<Self, (ParsingError, &str)> {
                $(
                    if value == stringify!($variant_name) {
                        return Ok(Self::$variant_name);
                    }
                )*
                return Err((ParsingError::Other("Invalid value"), value));
            }
        }
    }
}

macro_rules! QuirksParser {
    (
        $(#[$a:meta])*
        pub struct $name:ident {
            $(
                $(#[$attr:meta])*
                pub $field_name:ident : $field_type:ty,
            )*
        }
    ) => {
        $(#[$a])*
        pub struct $name {
            $(
                $(#[$attr])*
                pub $field_name: $field_type,
            )*
        }
        
        impl $name {
            pub fn parse(value: &str, mut base: Quirks)
                -> Result<Self, (ParsingError, &str)>
            {
                fn parse_one<'a>(v: &'a str)
                    -> Result<[&'a str; 2], (ParsingError, &'a str)>
                {
                    match &v.split("=").collect::<Vec<_>>()[..] {
                        [name, value] => Ok([name, value]),
                        _ => Err(
                            (ParsingError::Other("Not a name=value pair"), v)
                        ),
                    }
                }
                for parsed in value.split(',').map(parse_one) {
                    let [name, value] = parsed?;
                    match name {
                        $(
                            stringify!($field_name) => {
                                base.$field_name = <$field_type>::parse(value)?;
                            },
                        )*
                        other => {
                            return Err((ParsingError::InvalidQuirk, other));
                        },
                    }
                }
                Ok(base)
            }
            
            pub fn names() -> &'static [&'static str] {
                &[
                    $( stringify!($field_name), )*
                ]
            }
        }
    }
}

#[derive(Debug)]
pub enum ParsingError {
    InvalidQuirk,
    Other(&'static str),
}

impl fmt::Display for ParsingError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            Self::InvalidQuirk => {
                writeln!(f, "Invalid quirk. Allowed quirks:")?;
                for name in Quirks::names() {
                    writeln!(f, "    {}", name)?;
                }
                writeln!(f, "that's all.")?;
            },
            Self::Other(msg) => writeln!(f, "{}", msg)?,
        };
        Ok(())
    }
}

ParseableEnum!{
    #[derive(Clone, Debug)]
    pub enum MultiByteHandling {
        R16,
        /// Lowest common denominator
        R16AsG8R8,
    }
}

QuirksParser! {
    /// Stores hardware peculiarities.
    #[derive(Clone, Debug)]
    pub struct Quirks {
        pub multi_byte_handling: MultiByteHandling,
    }
}

pub const MOST_COMPATIBLE: Quirks = Quirks {
    multi_byte_handling: MultiByteHandling::R16AsG8R8,
};

pub const MOST_PERFORMANT: Quirks = Quirks {
    multi_byte_handling: MultiByteHandling::R16,
};