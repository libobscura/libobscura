/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Launches the GPU processing part of crispy. */

pub mod processing;
pub mod quirks;

use clap::ValueEnum;
use crate::quirks::Quirks;
use crispy::egl::DmabufImage;
use crispy::egl::headless;
use crispy::egl::headless::EglContext;
use crispy::error_backtrace;
use error_backtrace::{GenericError, Result as TraceResult};
use image;
use image::{DynamicImage, GenericImageView};
use std::error;
use std::fmt;
use std::fs::File;
use std::io;
use std::io::{Read, BufReader};
use std::path::{Path, PathBuf};
use thiserror::Error;
use tiff;
use tiff::encoder::TiffEncoder;


#[derive(Clone, Debug, Copy)]
pub enum Bits {
    B8,
    B10,
    B16,
}

#[derive(Copy, Clone, ValueEnum)]
#[clap(rename_all="verbatim")]
pub enum PixOrder {
    RGGB = 0,
    GRBG = 1,
    GBRG = 2,
    BGGR = 3,
}

impl From<PixOrder> for crispy::bayer::PixOrder {
    fn from(v: PixOrder) -> Self {
        use crispy::bayer::PixOrder::*;
        match v {
            PixOrder::RGGB => RGGB,
            PixOrder::GRBG => GRBG,
            PixOrder::GBRG => GBRG,
            PixOrder::BGGR => BGGR,
        }
    }
}

#[derive(Clone, Debug)]
pub struct OutInfo {
        pub width_px: u32,
        pub height_px: u32,
        /// Bits per pixel
        pub bpp: Bits,
}

pub type RawInfo = OutInfo;

impl fmt::Display for OutInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        <Self as fmt::Debug>::fmt(self, f)
    }
}

impl OutInfo {
    pub fn parse_str(s: &str)
        -> Result<Self, Box<dyn error::Error + Send + Sync + 'static>>
    {
        fn split<'a>(s: &'a str, chr: &[char]) -> Option<[&'a str; 3]> {
            s.split(chr).collect::<Vec<_>>().try_into().ok()
        }
        
        let [w, h, b] = split(s, &[':', ',', 'x', 'X', '×'])
            .ok_or("Failed to parse")?;

        Ok(RawInfo {
            width_px: u32::from_str_radix(w, 10)?,
            height_px: u32::from_str_radix(h, 10)?,
            bpp: match b {
                "8" => Ok(Bits::B8),
                "10" => Ok(Bits::B10),
                "16" => Ok(Bits::B16),
                _ => Err("Depth not supported"),
            }?,
        })
    }
}

pub struct InputInfo {
    pub dimensions: (u32, u32),
    pub bpp: Bits,
}

pub enum ImageInfo {
    Input(InputInfo),
    Output(OutInfo),
}

pub enum PixelSourceU8<R: Read> {
    File(R),
    Image(image::GrayImage),
}

impl<'a, R: Read + 'a> PixelSourceU8<R> {
    fn into_pixels(self) -> Box<dyn Iterator<Item=Result<u16, io::Error>> + 'a> {
        match self {
            Self::File(reader) => Box::new(
                reader.bytes().map(|b| b.map(|v| v as u16))
            ),
            Self::Image(img) => Box::new(
                // We can do into_vec safely even though it returns subpixels,
                // because each pixel in grayscale is only 1 subpixel.
                img.into_vec().into_iter().map(|b| Ok(b as u16))
            ),
        }
    }
}

pub enum PixelSourceU16<R: Read> {
    File(R),
    // FIXME: untested
    Image(image::ImageBuffer<image::Luma<u16>, Vec<u16>>),
}

struct TwoBytes<I: Iterator<Item=Result<u8, io::Error>>>(I);

impl<I: Iterator<Item=Result<u8, io::Error>>> Iterator for TwoBytes<I> {
    type Item = Result<u16, io::Error>;
    
    fn next(&mut self) -> Option<Self::Item> {
        let first = self.0.next()?;
        match first {
            Err(e) => Some(Err(e)),
            Ok(first) => match self.0.next()? {
                Err(e) => Some(Err(e)),
                Ok(second) => Some(Ok(
                    u16::from_le_bytes([first, second])
                )),
            }
        }
    }
}

impl<'a, R: Read + 'a> PixelSourceU16<R> {
    fn into_pixels(self)
        -> Box<dyn Iterator<Item=Result<u16, io::Error>> + 'a>
    {
        match self {
            Self::File(reader)
                => Box::new(TwoBytes(reader.bytes())),
            Self::Image(img)
            // We can do into_vec safely even though it returns subpixels,
            // because each pixel in grayscale is only 1 subpixel.
                => Box::new(img.into_vec().into_iter().map(|b| Ok(b))),
        }
    }
}

/// Grayscale pixel source
pub enum PixelSource<R: Read> {
    Bpp8(PixelSourceU8<R>),
    Bpp16(PixelSourceU16<R>),
}

impl<'a, R: Read + 'a> PixelSource<R> {
    /// Returns an iterator of u16 values, each is one pixel, even if the actual pixel was smaller than u16 - for interface simplicity.
    fn into_pixels(self)
        -> Box<dyn Iterator<Item=Result<u16, io::Error>> + 'a>
    {
        match self {
            Self::Bpp8(ps) => ps.into_pixels(),
            Self::Bpp16(ps) => ps.into_pixels(),
        }
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Read error")]
    Io(#[from] io::Error),
    #[error("Unsupported input")]
    Image(#[from] image::ImageError),
    #[error("Unsupported color configuration")]
    UnsupportedColor(image::ColorType, &'static str),
}


pub fn read_format(format: Option<OutInfo>, input: &Path) -> Result<(ImageInfo, PixelSource<impl Read>), Error> {
    match format {
        Some(info) => {
            let reader = BufReader::new(File::open(input)?);
            let source = match info.bpp {
                Bits::B8 => PixelSource::Bpp8(PixelSourceU8::File(reader)),
                Bits::B10 | Bits::B16 => PixelSource::Bpp16(PixelSourceU16::File(reader)),
            };
            Ok((ImageInfo::Output(info), source))
        },
        None => match image::open(input)? {
            DynamicImage::ImageLuma8(img) => Ok((
                ImageInfo::Input(InputInfo {
                    dimensions: img.dimensions(),
                    bpp: Bits::B8,
                }),
                PixelSource::Bpp8(PixelSourceU8::Image(img)),
            )),
            DynamicImage::ImageLuma16(img) => Ok((
                ImageInfo::Input(InputInfo {
                    dimensions: img.dimensions(),
                    bpp: Bits::B16,
                }),
                PixelSource::Bpp16(PixelSourceU16::Image(img)),
            )),
            dynamic @ DynamicImage::ImageRgba8(_) => Ok((
                ImageInfo::Input(InputInfo {
                    dimensions: dynamic.dimensions(),
                    bpp: Bits::B8,
                }),
                PixelSource::Bpp8(PixelSourceU8::Image(dynamic.into_luma8())),
            )),
            other => Err(Error::UnsupportedColor(other.color(), "only 8-bit rgba and gray supported")),
        }
    }
}

#[derive(Error, Debug)]
pub enum QuirksError {
    #[error("Invalid quirks specification in {0:?}: {1}")]
    InvalidQuirks(quirks::ParsingError, String),
}

pub fn parse_quirks(quirks: Option<&str>) -> Result<Quirks, QuirksError> {
    match quirks {
        None => Ok(quirks::MOST_COMPATIBLE),
        Some(quirks) => quirks::Quirks::parse(&quirks, quirks::MOST_COMPATIBLE)
            .map_err(|(e, substring)| QuirksError::InvalidQuirks(e, substring.into())),
    }
}

pub fn get_bgra_tiff_writer(output: PathBuf, dimensions: (u32, u32)) -> impl Fn(&[u8]) -> TraceResult<(), GenericError> {
    move |outbuf| {
        let mut outbuf = outbuf.chunks(4);
        
        write_bgra_pixels_to_tiff(
            &mut || outbuf.next(),
            dimensions,
            output.as_path(),
        ).map_err(GenericError)?;
        Ok(())
    }
}

fn create_gbm_dmabuf(
    egl: &headless::ContextRef,
    (width, height): (u32, u32),
    format: gbm::Format,
    setup: impl FnOnce(gbm::BufferObject<()>)
        -> Result<gbm::BufferObject<()>, Box<dyn error::Error>>,
) -> Result<crispy::egl::OwnedDmabufImage, Box<dyn error::Error>> {
    let gdev = egl.get_device();
    let bo = gdev.create_buffer_object::<()>(
        width,
        height,
        format,
        gbm::BufferObjectFlags::RENDERING | gbm::BufferObjectFlags::LINEAR,
    )?;
    let bo = setup(bo)?;
    Ok(headless::import_dmabuf(egl, bo.fd()?.into(), (width, height), format)?)
}

fn create_gbm_output_dmabuf(egl: &headless::ContextRef, dimensions: (u32, u32))
    -> Result<crispy::egl::OwnedDmabufImage, Box<dyn error::Error>>
{
    create_gbm_dmabuf(egl, dimensions, gbm::Format::Argb8888, |b| Ok(b))
}

use crispy::glium;

pub fn draw<R: Read>(
    egl: headless::ContextRef,
    reader: PixelSource<R>,
    source_bpp: Bits,
    quirks: Quirks,
    // In pixels of the input texture
    (source_width, source_height): (u32,u32),
    (width_px, height_px): (u32, u32),
    // FIXME: this is too complicated
    render: impl FnOnce(
        &headless::ContextRef,
        &DmabufImage,
        &mut glium::framebuffer::SimpleFrameBuffer,
        Bits,
        Quirks,
        (u32, u32)
    ) -> TraceResult<(), GenericError>,
    handler: &mut dyn FnMut(&[u8]) -> TraceResult<(), GenericError>,
) -> TraceResult<(), GenericError> {
    let _context_lock = egl.make_current();
    let gpubuf_bpp = source_bpp;
    
    let (px_per_sample, fourcc) = match gpubuf_bpp {
        Bits::B8 => (1, gbm::Format::R8),
        Bits::B10 | Bits::B16 => match quirks.multi_byte_handling {
            quirks::MultiByteHandling::R16
                => (1, gbm::Format::R16),
            quirks::MultiByteHandling::R16AsG8R8
                // Not a typo. GBM will not allocate Rg88.
                => (1, gbm::Format::Gr88),
        },
    };
    
    let in_tex = create_gbm_dmabuf(
        &egl,
        (source_width * px_per_sample, source_height),
        fourcc,
        |mut bo| {
            let mut pixels_seen = 0;
            let pixel_count = source_width as usize * source_height as usize;
            bo.map_mut(
                0,
                0,
                source_width,
                source_height,
                |inbuf| {
                    let inbuf = inbuf.buffer_mut();
                    for (i, pixel) in reader.into_pixels().take(pixel_count).enumerate() {
                        let red = pixel.unwrap();
                        match gpubuf_bpp {
                            Bits::B8 => { inbuf[i] = red as u8 },
                            Bits::B10 | Bits::B16 => {
                                inbuf[i * 2..][..2].copy_from_slice(&red.to_le_bytes());
                            },
                        }
                        pixels_seen += 1;
                    }
                },
            )?;
            if pixels_seen < pixel_count {
                eprintln!(
                    "Warning: {} pixels were available, but buffer needs {} pixels",
                    pixels_seen,
                    pixel_count,
                );
            }
            Ok(bo)
        },
    ).map_err(GenericError)?;

    let out_tex = create_gbm_output_dmabuf(&egl, (width_px, height_px))
        .map_err(GenericError)?;
    let facade = crispy::Facade::new(egl.clone(), (width_px, height_px));
    let mut target = crispy::shaders::import_target_texture(&facade, &out_tex.get_texture().get_texture());
    let mut target = crispy::shaders::texture_to_surface(&facade, &mut target)?;
    
    render(
        &egl,
        &in_tex.get_texture(),
        &mut target,
        gpubuf_bpp,
        quirks,
        (width_px, height_px),
    )?;
    let map = out_tex.dmabuf().memory_map_ro()?;
    handler(map.as_slice())
}

/// FIXME: this might crash on buffers of length not divisible by 4
pub fn write_bgra_pixels_to_tiff<'a>(
    source: &mut impl FnMut() -> Option<&'a [u8]>,
    (width_px, height_px): (u32, u32),
    output: &Path,
) -> Result<(), Box<dyn error::Error>> {
    let mut data: Vec<u8> = Vec::with_capacity(width_px as usize * height_px as usize * 4);
    {
        while let Some(pixel) = source() {
            if let &[b, g, r, a] = pixel {
                data.extend_from_slice(&[r, g, b, a]);
            } else {
                panic!();
            }
        }
    }
    
    let mut o = File::create(output)?;
    let mut o = TiffEncoder::new(&mut o)?;
    o.write_image::<tiff::encoder::colortype::RGBA8>(
        width_px as u32,
        height_px as u32,
        &data,
    )?;
    Ok(())
}
