/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Runs conversion shaders on dummy test data.
 * 
 * Because crispy doesn't define a backend or input procedures, those are the only integration tests for crispy.
 * 
 * All of the tests here use GBM buffers.
 */

// This file is separate from all other tests  because tests may need GPU availability to run, so it needs to be invoked separately.

use crispy::egl::headless;
use crispy::yuv;
use crispyconv::{draw, Bits, PixOrder, PixelSource, PixelSourceU8};
use crispyconv::processing::{render_bayer, render_yuv};
use crispyconv::quirks;
use std::io::Read;


struct Bright;
impl Read for Bright {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        buf.fill(255);
        Ok(buf.len())
    }
}

#[test]
fn bayer8() {
    let egl = headless::ContextRef::new().unwrap();
    draw(
        egl,
        PixelSource::Bpp8(PixelSourceU8::File(Bright)),
        Bits::B8,
        quirks::MOST_COMPATIBLE,
        (256u32, 256u32),
        (256u32, 256u32),
        |egl, source, target, gpubuf_bpp, quirks, dimensions| 
            render_bayer(egl, source, target, gpubuf_bpp, quirks, dimensions, PixOrder::GRBG),
        &mut |out| {
            dbg!(&out[0..4]);
            // FIXME: 2 values are allowed because the test differs between GPUs (Intel HD4000 and AMD Vega). This is because calculations are done with floats: the Bayer shader gains speed by interpolating values. This probably causes precision issues. Root cause is probably different truncation or the number of precision bits.
            if &out[0..4] == &[175, 185, 255, 255][..] || &out[0..4] == &[174, 185, 255, 255][..] {
                // good
            } else {
                assert_eq!(&out[0..4], &[175, 185, 255, 255][..]);
            }
            Ok(())
        },
    ).unwrap();
}


#[test]
fn yuyv() {
    let egl = headless::ContextRef::new().unwrap();
    draw(
        egl,
        PixelSource::Bpp8(PixelSourceU8::File(Bright)),
        Bits::B8,
        quirks::MOST_COMPATIBLE,
        (512u32, 256u32),
        (256u32, 256u32),
        |egl, source, target, gpubuf_bpp, quirks, dimensions| 
            render_yuv(egl, source, target, gpubuf_bpp, quirks, dimensions, yuv::ColorSpace::BT709),
        &mut |out| {
            assert_eq!(&out[0..4], &[255, 179, 255, 255][..]);
            Ok(())
        },
    ).unwrap();
}