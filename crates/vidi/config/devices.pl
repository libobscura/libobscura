% This is libvidi device configuration using a Prolog-like syntax.
%
% `padformat` describes available pad formats
% padformat(
%    Device, % raw device name, converted to an atom. Replace all spaces with "_" and prepend "dev_"
%    Padindex, % relative to the device
%    MbusCode) % Textual form from the `v4l2_subdev::MediaBusFmt` enum
%
% `padres` describes available pad resolutions at a given format
% padres(
%       Padformat, % the selected pad and format
%       Width, % in pixels
%       Height)


% Librem 5, main camera

padformat(dev_s5k3l6xx, 0, mbus_SGRBG8_1X8).
padres(padformat(dev_s5k3l6xx, 0, mbus_SGRBG8_1X8), 1052, 780).
padres(padformat(dev_s5k3l6xx, 0, mbus_SGRBG8_1X8), 2104, 1560).
padres(padformat(dev_s5k3l6xx, 0, mbus_SGRBG8_1X8), 4208, 3120).

source(
    dev_s5k3l6xx_3_002d,
    0, Mbus, W, H
) :- padres(padformat(dev_s5k3l6xx, 0, Mbus), W, H).

processing(
    dev_imx8mq_mipi_csi2_30b60000_csi,
    0, format(Mbus, W, H),
    1, format(Mbus, W, H)
).

processing(
    dev_csi,
    0, format(Mbus, W, H),
    1, format(Mbus, W, H)
).

output(
    dev_csi_capture,
    format(Mbus, W, H),
    Fourcc
) :-
    imx7_csi(Mbus, Fourcc).

% Results obtained on the Librem 5 with an i.MQ8MQ SoC, by querying for all mbus formats while unconfigured.
% Formats listed using the "video_formats" tool from "vidi-tools".
% This list may not be exhaustive.
imx7_csi(mbus_Y8_1X8, fourcc_GREY).
imx7_csi(mbus_UYVY8_2X8, fourcc_UYVY).
imx7_csi(mbus_YUYV8_2X8, fourcc_YUYV).
imx7_csi(mbus_Y10_1X10, fourcc_Y10 ).
imx7_csi(mbus_Y12_1X12, fourcc_Y12 ).
imx7_csi(mbus_Y14_1X14, fourcc_Y14 ).
imx7_csi(mbus_UYVY8_1X16, fourcc_UYVY).
imx7_csi(mbus_YUYV8_1X16, fourcc_YUYV).
imx7_csi(mbus_SBGGR8_1X8, fourcc_BA81).
imx7_csi(mbus_SGBRG8_1X8, fourcc_GBRG).
imx7_csi(mbus_SGRBG8_1X8, fourcc_GRBG).
imx7_csi(mbus_SRGGB8_1X8, fourcc_RGGB).
imx7_csi(mbus_SBGGR10_1X10, fourcc_BG10).
imx7_csi(mbus_SGBRG10_1X10, fourcc_GB10).
imx7_csi(mbus_SGRBG10_1X10, fourcc_BA10).
imx7_csi(mbus_SRGGB10_1X10, fourcc_RG10).
imx7_csi(mbus_SBGGR12_1X12, fourcc_BG12).
imx7_csi(mbus_SGBRG12_1X12, fourcc_GB12).
imx7_csi(mbus_SGRBG12_1X12, fourcc_BA12).
imx7_csi(mbus_SRGGB12_1X12, fourcc_RG12).
imx7_csi(mbus_SBGGR14_1X14, fourcc_BG14).
imx7_csi(mbus_SGBRG14_1X14, fourcc_GB14).
imx7_csi(mbus_SGRBG14_1X14, fourcc_GR14).
imx7_csi(mbus_SRGGB14_1X14, fourcc_RG14).
