# SPDX-FileCopyrightText: 2024 DorotaC
#
# SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later

/* TODO: frame rate and stuff.
TODO: this must be further abstracted into pads and maybe streams when a pad is multiplexed.
TODO: how to deal with immutable links? Add an automatic tracersal clause?
TODO maybe: carry tags, like "accurate", "fast", "CPU", "GPU".
*/

% conversion rules, should be defined by the driver developer. Here there's only a single format.
node("sens", none, format(640, 480, yuyv)).
node("conv", format(640, 480, yuyv), format(640, 480, rgb)).
node("output", format(640, 480, rgb), none).


% topology
link("sens", "conv").
link("sens", "output").
link("conv", "output").
sensor("sens").
output("output").


% pathfinding
find_sensor(N) :-
    sensor(X),
    N = node(X, _, _),
    N.


find_route(Goal, Path) :- next_node(Goal, Path).

% this naively traverses the graph, depth-first
next_node(Goal, [], FinalPath) :-
    find_sensor(N),
    next_node(Goal, [N], FinalPath).


next_node(Goal, [Prev|Path], FinalPath) :-
    Prev = node(PName, _, Format),
    N = node(NName, Format, _),
    N,
    link(PName, NName),
    (
        (
            Goal = Format,
            output(NName),
            FinalPath = [N|[Prev|Path]]
        );
        (
            next_node(Goal,  [N|[Prev|Path]], FinalPath)
        )
    ).

/* The path starts with the last node.

?- next_node(L, [], P).
L = format(640, 480, rgb),
P = [node("output", format(640, 480, rgb), none), node("conv", format(640, 480, yuyv), format(640, 480, rgb)), node("sens", none, format(640, 480, yuyv))] ;
*/