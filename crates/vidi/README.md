Vidi-rs is a camera API for Linux
========

Vidi supports cameras using the [Video 4 Linux](https://docs.kernel.org/userspace-api/media/v4l/v4l2.html) and [Linux Media Infrastructure](https://docs.kernel.org/userspace-api/media/index.html) APIs.

Vidi itself is an experiment to create hard to misuse APIs that people can easly use to 

- make camera applications, and
- support new camera models.

Vidi is inspired by and wouldn't be possible without [libmegapixels](https://gitlab.com/megapixels-org/libmegapixels) and [libcamera](https://libcamera.org/).


Demo
-------

The binaries are in src/bin/.

```
cargo run --bin camera_get
```

camera_get requires an UVC camera and demonstrates the flow of:

1. Camera info comes from udev.
2. User requests a camera which is created on demand.
3. How another handle to the same camera cannot be acquired despite no global camera registry.

More examples (OpenGL, easy buffer API) are in [vidi-examples](../vidi-examples).

Using
----

Your system needs some native packages. On Fedora:

```
dnf install libudev-devel clang-devel libglvnd-devel libgbm-devel
```

Add the libobscura repository to your project's `.cargo/config.toml` (or in your home directory):

```
[registries.libobscura]
index = "sparse+https://codeberg.org/api/packages/libobscura/cargo/"
```

and add the dependency to your project's `Cargo.toml`:

```
[dependencies]
vidi = { version = "0.2", registry = "libobscura" }
```

Challenges
----

### Point-and-shoot (in progress)

The API is as easy as this (see [vidi-examples](../vidi-examples/)):

```
let cameras_list = vidi::actors::camera_list::spawn()?;
let cameras = cameras_list.cameras();
let camera = cameras_list.create(&cameras[0].info.id)
    .expect("No such camera")
    .expect("Failed to create camera");

let mut camera = camera.acquire();
if let Ok(ref mut camera) = camera {
    let mut stream = camera.start(
        Config{ fourcc: FourCC::new(b"YUYV"), width: 640, height: 480 },
        4
    ).unwrap();
    let (buf, meta, _next) = stream.next().unwrap();
    let mmap = buf.memory_map_ro().unwrap();
    let data = mmap.as_slice();
}
```

### Easy new camera support (in progress)

Every device is just a configuration file specifying how to configure the image processing pipeline. To the extent possible, the heavy lifting like configuring the image processing pipeline is done by the library core, so new device backends only have to implement a few basic methods, like `start()`, `stop()`, `configure()`, etc.

TODO: support more complicated devices.

### No race conditions, no segfaults. (DONE)

This is taken care of by using Rust throughout the library. Rust encourages [RAII](https://en.cppreference.com/w/cpp/language/raii) and discourages objects being in an invalid state. It also has especially safe handling of concurrency built in.

### Informative errors (in progress)

Rust makes informative errors possible and easy with the [Result](https://doc.rust-lang.org/std/result/enum.Result.html) type, and supporting crates like [thiserror](https://docs.rs/thiserror/latest/thiserror/), [anyhow](https://docs.rs/anyhow/latest/anyhow/), even allowing exception-free [backtrace](https://lib.rs/crates/backtrace-error)

### Image processor (in progress)

Applications like the browser expect RGB data. Cameras normally provide YUV or Bayer data, so a builtin processing step is needed.

### Auto -contrast, -brightness, -focus

More advanced cameras may expose only low-level controls, leaving the task of managing the image to the driver software.

Optional challenges
----

### C ABI

A C ABI will not be memory-safe, but might still be useful for some people.

### Constraint-based pipeline configuration

Some devices have complicated image processing pipelines, allowing many output formats, sizes, and qualities. What if the application could express a set of constraints, e.g. "size at least 640×480, and either RGB or YUV data"? Perhaps the library can figure out how to configure the pipeline without any extra input.

For the SQL-inclined, the idea in pseudocode:

```
let configurations = camera.get_configs("SELECT * FROM possible_configs where (format == RGB OR format == YUV) AND width >= 640 AND height >= 480")?;
```

Progress
----

- USB camera support (in progress)
- Librem 5 cameras support
- Pipewire integration

Design
------

For more technical elaboration, see [doc/design.md](doc/design.md)

License
----

Vidi-rs is distributed under the terms of the LGPL2.1 or later, at your choice. See [COPYING.md](COPYING.md).

If you distribute a modified version of vidi-rs, you **must share your modifications**. (The licenses, MPL 2.0 or LGPL 2.0 or LGPL 3.0, require this).