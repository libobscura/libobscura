/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! UVC (USB) cameras.

This doesn't actually have any UVC-specific functionality and the API is currently a mistake.

In the future, this should only control the special UVC controls processing and nothing else.
Meaning: the device and sensor trickery don't need to be here.*/

// FIXME: move generic v4l2 handling to `super` module. But maybe after having more drivers to understand what interfaces need to be exposed.

use crate::actors::camera_list::CreationKit;
use crate::actors::watcher_udev::Device;
use crate::{pipelines, search};
use crate::util::flock::Locked;
use crate::util::media;
use media_subsystem::{MediaV2Entity, EntityName};
use parking_lot::Mutex;
use std::error::Error;
use std::io;
use std::ops::DerefMut;
use std::sync::Arc;
use super::CameraInfo;
use tracing::debug;
use v4l;


struct CameraDevice {
    // TODO: the media device is not a video capture device.
    device: media::Device,
    sensor_id: media_subsystem::EntityId,
}

impl super::UnacquiredCameraImpl for CameraDevice {
    fn acquire(self: Box<Self>)
        -> Result<
            Arc<Mutex<dyn super::CameraImpl>>,
            Box<dyn Error>,
        >
    {
        let topology = self.device.get_topology()?;
        // FIXME: acquire the sensor and all other entities in the pipeline as well
        // FIXME: don't lock the entire media device. Let other applications take pipelines on the same device in parallel
        if let Ok(device) = self.device.try_lock() {
            let sensor_entity = topology.0.entities.iter()
                .find(|e| e.id == self.sensor_id).unwrap()
                .clone();

            let database = search::TopologyDatabase::<_>::new(&topology);
            let interfaces = search::video_capture_interfaces(
                &mut media::Io,
                &database,
                self.sensor_id,
            );
            let interface = interfaces.get(0)
                .ok_or(io::Error::other("No video capture interfaces. Did one disappear?"))?;
            let video_capture_entity = search::entity_for_interface(&database, interface.id)
                .expect("One must exist, because we find the interface from the entity in the first place.")
                .clone();
            let video_capture_device = v4l::Device::with_path(media::Io.interface_find_path(interface)?)?;
            Ok(Arc::new(Mutex::new(Camera {
                device,
                video_capture_device,
                video_capture_entity,
                sensor_entity,
            })))
        } else {
            Err(Box::new(super::AcquireError::AlreadyAcquired))
        }
    }
}

// TODO: store all devices used in the image path
struct Camera {
    /// Keeps the media device exclusively acquired for modification.
    // TODO: multiple cameras may use the same media device with dferent paths (IPU3), so locking must be redesigned
    device: Locked<media::Device>,
    video_capture_device: v4l::Device,
    video_capture_entity: MediaV2Entity,
    sensor_entity: MediaV2Entity,
    // TODO: sensor_device: mediia::Device,
}

impl super::CameraImpl for Camera {
    fn video_capture_device(&self) -> &v4l::Device {
        &self.video_capture_device
    }
    /*
    fn get_knowledge(&self) -> Result<Knowledge, io::Error> {
        let topology = self._device.get_topology()?;
        let database = 
    }*/
    fn media_device(&self) -> &media::Device {
        &self.device
    }
    fn media_device_mut(&mut self) -> &mut media::Device {
        self.device.deref_mut()
    }
    
    
    fn video_capture_entity(&self) -> &MediaV2Entity {
        &self.video_capture_entity
    }
    fn sensor_entity(&self) -> &MediaV2Entity {
        &self.sensor_entity
    }
}

macro_rules! try_soft {
    ($result:expr, $debug:expr) => {
        match $result {
            Ok(r) => r,
            Err(e) => {
                $debug(e);
                return Vec::new();
            }
        }
    };
}

pub fn check_match(device: &Device) -> Vec<CreationKit> {
    let device_path = device.device_node.as_ref();
    let device_path = if let Some(device_path) = device_path {
        device_path
    } else {
        debug!("Device without a node path: {:?}", device);
        return Vec::new();
    };
    
    let d = try_soft!(media::Device::new(device_path), |e| debug!(
        "Failed to open device {:?}: {:?}",
        device_path,
        e,
    ));
    let deviceinfo = try_soft!(d.get_device_info(), |e| debug!(
        "Not a media device {:?}: {:?}",
        device_path,
        e,
    ));
    let mediadevice = d;

    let topology = try_soft!(mediadevice.get_topology(), |e| debug!(
        "Failed to get topology for {:?}: {:?}",
        device_path,
        e,
    ));
    topology.get_sensors()
        .filter_map(|sensor| {
            let database = search::TopologyDatabase::<_>::new(&topology);
            let mut outputs = search::outputs(&database, sensor.id).into_iter();
            if let Some(output) = outputs.next() {
                if let Some(_) = outputs.next() {
                    debug!("Taking first output from sensor {:?}", sensor);
                }
                Some((sensor, output))
            } else {
                debug!("No output found from sensor {:?}", sensor);
                None
            }
        })
        .map(|(sensor, _output)| CreationKit {
            info: CameraInfo {
                device: device.clone(),
             // FIXME: placing ":" in device name and sensor name could cause duplicates
                id: format!(
                    "{}:{}:{:0x}:{}",
                    deviceinfo.get_driver(),
                    deviceinfo.get_device(),
                    device.stable_id(),
                    // FIXME: convert name to hex if raw
                    match &sensor.name {
                        EntityName::Text(s) => s.as_str(),
                        EntityName::Bytes(_) => "",
                    },
                ),
                sensor: sensor.name.clone(),
            },
            builder: build,
        })
        .collect()
}

pub fn build(camera: CameraInfo)
    -> Result<pipelines::UnacquiredCamera, Box<dyn Error>>
{
    let device = media::Device::new(
        camera.device
            .device_node.as_ref()
            .unwrap() // Devices without a node name don't pass check_match.
    )?;
    
    let topology = device.get_topology()?;
    let sensor = topology.0
        .entities.iter()
        .find(|e| e.name == camera.sensor)
        .ok_or(io::Error::other("No sensor with this name. Did it disappear?"))?;
    
    Ok(pipelines::UnacquiredCamera {
        device: Box::new(CameraDevice {
            device,
            sensor_id: sensor.id,
        }),
        id: camera.id,
    })
}