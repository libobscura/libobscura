/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Acquire cameras.

TODO: this should be a test*/

use vidi as camera;
use std::io;

fn main() -> io::Result<()> {
    let cameras_list = camera::actors::camera_list::spawn().unwrap();
    let cameras = cameras_list.cameras();
    dbg!(&cameras);
    let camera = cameras_list.create(&cameras[0].info.id())
        .unwrap().unwrap();
    dbg!(camera.get_id());
    // FIXME: test, fail to acquire the same camera again.
    {
        let q = camera.acquire();
        let camera2 = cameras_list.create(&cameras[0].info.id())
        .unwrap().unwrap();
        dbg!(camera2.acquire().is_ok());
        dbg!(q.is_ok());
    }
    let camera = cameras_list.create(&cameras[0].info.id())
        .unwrap().unwrap();
    dbg!(camera.acquire().is_ok());
    Ok(())
}