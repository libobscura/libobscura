/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Try to acquire the same camera twice, but from two independent resources.
This needs a global lock to work.

TODO: this should be a test
*/

use vidi as camera;
use std::io;

fn main() -> io::Result<()> {
    let cameras_list = camera::actors::camera_list::spawn()?;
    
    let cameras = cameras_list.cameras();
    dbg!(&cameras);
    let camera = cameras_list.create(&cameras[0].info.id())
        .unwrap().unwrap();
    dbg!(camera.get_id());
    // FIXME: test, fail to acquire the same camera again.
    {
        let _q = camera.acquire();
        let cameras_list = camera::actors::camera_list::spawn()?;
        let camera2 = cameras_list.create(&cameras[0].info.id())
        .expect("No such camera").unwrap();
        assert!(camera2.acquire().is_err());
    }
    Ok(())
}