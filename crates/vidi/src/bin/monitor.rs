/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

use mio;
use std::io;
use udev;


fn main() -> io::Result<()> {
    let mut socket = udev::MonitorBuilder::new()?
        // .match_subsystem_devtype("usb", "usb_device")?
  //      .match_subsystem_devtype("block", "disk")?
        .listen()?;

    let mut poll = mio::Poll::new()?;
    let mut events = mio::Events::with_capacity(1024);
    poll.registry().register(
        &mut socket,
        mio::Token(0),
        mio::Interest::READABLE | mio::Interest::WRITABLE,
    )?;

    loop {
        poll.poll(&mut events, None)?;
        for ev in socket.iter() {
            dbg!(ev);
        }
    }
    unreachable!();
}