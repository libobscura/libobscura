/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! List cameras */

use vidi;
use std::io;
use std::thread;
use std::time::Duration;

fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    dbg!(cameras_list.cameras());
    thread::sleep(Duration::from_secs(4));
    if let Err(e) = cameras_list.stop() {
        dbg!(e);
    }
    Ok(())
}