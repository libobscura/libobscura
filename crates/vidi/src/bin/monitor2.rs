/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Shorter version of the camera monitor */

use vidi as camera;
use std::io;
use std::thread;
use std::time::Duration;

fn main() -> io::Result<()> {
    let watcher = camera::actors::watcher_udev::Watcher::spawn(|ev| { dbg!(ev); })?;
    thread::sleep(Duration::from_secs(4));
    if let Err((_watcher, e)) = watcher.stop() {
        dbg!(e);
    }
    Ok(())
}