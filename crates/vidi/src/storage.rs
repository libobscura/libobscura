/* Copyright (C) 2025 DorotaC
 * SPDX-License-Identifier: LGPL-2.1-or-later OR MPL-2.0
 */

/*! Manages the loading of device configurations from storage.

Actual device configurations are difficult to figure out from the kernel interface, so intead we're going to ignore that interface.
Instead, we'll read the kernel sources with the rules written out explicitly and dstribute those rules alongside the library.

Rules are still loadable from a file if the user wants to modify them.*/

use std::env;
use std::fs::File;
use std::io::{self, Read};
use std::path::PathBuf;

const CONFIG_NAME: &'static str = "devices.pl";
const VAR_NAME: &'static str = "LIBOBSCURA_DEVICES_DIR";

pub trait Io {
    /// Reads out predicates describing V4L2 subdevices stored in the config
    fn device_definitions<'a>(&'a mut self) -> impl Iterator<Item=(String, io::Result<String>)>;
}

/// Retrieves config stored in a directory under LIBOBSCURA_DEVICES_DIR, ///
/// The env var exists to load the files from a system path after packaging and deployment, while remaining open to administrator's modifications.
pub struct FileConfig;

impl FileConfig {
    fn env_file_path() -> Option<PathBuf> {
        let mut p = PathBuf::from(env::var(VAR_NAME).ok()?);
        p.push(CONFIG_NAME);
        Some(p)
    }

    /// Loads the configuration. On error, returns the configuration source that was unavailable.
    fn load() -> Result<Option<(String, File)>, (String, io::Error)> {
        if let Some(path) = Self::env_file_path() {
            let path_str = path.to_string_lossy().into_owned();
            match File::open(path) {
                Ok(file) => Ok(Some((path_str, file))),
                Err(e) => Err((path_str.clone(), e)),
            }
        } else if let Ok(_) = env::var("CARGO_RUN") {
            Ok(None)
        } else {
            Ok(None)
        }
    }
}

impl Io for FileConfig {
    fn device_definitions<'a>(&'a mut self) -> impl Iterator<Item=(String, io::Result<String>)> {
        let iter = match FileConfig::load() {
            Ok(Some((path, mut file))) => Some((
                path,
                {
                    let mut out = String::new();
                    file.read_to_string(&mut out).map(|_| out)
                }
            )),
            Ok(None) => None,
            Err((path, e)) => Some((path, Err(e))),
        };
        iter.into_iter()
    }
}