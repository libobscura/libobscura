Problems and solutions
----------------------

### Segfaults, race conditions, weird crashes

Rust prevents crashes from memory safety, but it does not present crashes from resources accessed when they are not in a ready state.

One way to improve this is to prevent resources from being used before initialized. A `Camera` instance does not have a `start()` method. An `AcquiredCamera` does.

Another way is to reduce the amount of stored state. A `Config` instance is a standalone object, which does not reference a `Camera`.

Race conditions are avoided by reducing sharing. Two `Camera` instances may refer to the same camera. They have redundant copies of the same resources. They share only a single "acquire" lock on the device file, rather than the entire state. This is enough to prevent the user from modifying the same camera from multiple places at the same time, while simplifying the ownership model: each camera can be freed independently.

As a side effect, there is no need for mutexes throughout the code base, other than the file lock.

Negative sides:
- more computational effort: creating a new instance means probing the camera again. No shared state means no cached state, and we're starting from zero every time a new resource is acquired. (This could be mitigated by caching some write-once, read-only init description data.)

#### Buffer management

Offloaded to libv4l-rs. There, the application does not own buffers, it only receives a reference.

TODO: verify if this approach works with an end-to-end demo.

#### Signals

Signals sent between threads are implemented as breakable channels. Either end can be freed independently. This way, attempting to send a signal to a destroyed object results in an error, rather than a crash as with callbacks in C.

For example, a `UdevWatcher` can attempt to send a `CameraChanged` signal to any endpoint. The library doesn't have to ensure that the endpoint exists. If it doesn't, it's only an error.

Negative sides:

- tight coupling between senders/receivers. Small impact: the pieces are not meant to be general purpose anyway.
- ad-hoc event loop.

#### Control flow, ownership, and signals

TODO: describe the details

![arch.svg](arch.svg)

### Pipeline matching

Vidi-rs is responsible for selecting the pipeline handler, based on information from the Linux media subsystem. The camera-specific pipeline handler then chooses an arbitrary ID for the device based on that data. It can open and probe the device as it pleases, using an internal convenience library.

This just makes pipeline selection uncomplicated.

TODO
-------

### Add a CameraManager

CamerasList and UdevWatcher must spawn at the same time, or we risk deadlocks. Either add CameraManager, or specialize CamerasList to manage UdevWatcher itself.

### Separate desired output mode from pipeline configuration

The user cares about the format of the data, whereas the library cares about how to achieve that.

Those should be stored as two separate sets of data. The desired format should contain a request. The camera-specific pipeline configuration should contain a description of the required pipeline graph (i.e. links and pads configuration data).

The configuration is camera-specific, but its lifetime shouldn't be tied to a camera instance, which would complicate using the configuration. One way to detach the lifetime from an instance would be to make it dependent on the physical resource instead. For example, by tying it to a serial number of the hardware.

```
struct Configuration {
     pub mode: FrameMode,
     /// Not actually storing any file descriptors. The files can be opened when needed, pad and link numbers are stable.
     pipeline_config: Graph<DevicePadFormat, LinkFromTo>,
     /// Derive the number from a stable identifier
     camera: CapabilityNumber,
}
```

A nice side effect from that is that the same config can be re-used between sessions.

### Camera-independent desired output mode

Another side effect from the above is that configuration is detached from the desired mode. The user might want to operate multiple cameras with the same parameters, and making the desire explicit allows that.

### One pipeline handler to rule them all

Vidi-rs limits itself to v4l devices and the media subsystem. All devices there are represented as directed graphs of media subdevices and pads, and links. Graphs can be queried.

This implies that a sufficiently smart algorithm can perform a search of possible paths through the graph to arrive at a set of desired outputs, e.g. resolution, format, the number of streams, the kind of statistics in one of the streams.

Adding an image processing component doesn't change the situation apart from adding extra nodes at the end.

TODO: Can the graph have loops? Does any device have loops in practice? Does it change the needed graph traversal strategy?

TODO: Adding a feedback loop for auto- algoritms only modifies the values of device controls, which are not part of the graph. Does that change the nature of the graph search?

The path candidates then can be applied to the device, to see which one actually results in a working pipeline.

This approach would make it easy for device vendors to add new devices, because they wouldn't have to worry about finding working configurations.

Platform vendors would have to present rules for what each node in the pipeline does, though, e.g. this device produces YUV frames, that device rescales the size by 1/2, etc. See the file `config_modes.pl` for a Prolog rule set.

This approch also allows API users to express desired settings as a query with multiple possible constraints and answers:

The "Can I use this X" query could be in the form of inputting a structure with each field optional, and returning a list with all fields filled in. SQL equivalent: `select * where X = something`. Prolog: `Answer = camera_modes(something, _, _), Answer.`

The "Which X values are supported" query could be in the form of inputting a structure with fields of interest marked. SQL equivalent: `select modes.X from modes`. Prolog: `modes(Answer, _, _).`.

### Or hardcode pipeline presets

Alternatively, do what libmegapixels does and require every vendor to hardcode a couple of pipeline presets: preview, photo, fast video, etc.
