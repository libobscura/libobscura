crispy-img
===

offers GPU accelerated image processing.

It can render from and into DMABUF buffers, avoiding unnecessary copies.

It can render directly on the screen as well.

Status
-----

The library is a proof-of-concept. It's possible to add new shaders, but it's not pretty.

Implemented:

- YUV2 to RGB
- debayering (testing needed)

![A painter looking at a canvas with 4 colored strokes: 2 colored, 2 gray, representing YUYV. The painter holds a brush at a canvas on the other side with 3 colored strokes: red, green, and blue.](../../doc/painter.png)

Figure: An artist's impression of a color conversion shader.

Using
----

Add the libobscura repository to your project's `.cargo/config.toml` (or in your home directory):

```
[registries.libobscura]
index = "sparse+https://codeberg.org/api/packages/libobscura/cargo/"
```

and add the dependency to your project's `Cargo.toml`:

```
[dependencies]
crispy = { version = "0.3", registry = "libobscura" }
```

Documentation
-----

Crispy-img is part of [libobscura](../../), look there for documentation.