/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Headless (GBM) EGL context.
*/

use crate::egl_ext as ext;
use dma_boom::DmaBuf;
use error_backtrace::ResultBacktrace;
use error_backtrace::{Result as TracedResult, IntoTraced};
use ext::ExtPlatformBaseInstance;
use gbm::AsRaw;
use khronos_egl as egl;
use std::ffi::c_void;
use std::fs;
use std::fs::File;
use std::io;
use std::ops::Deref;
use std::ptr;
use std::sync::Arc;
use std::sync::atomic;
use std::sync::atomic::AtomicUsize;
use super::DmabufImportError;
use super::OwnedDmabufImage;
use thiserror::Error;
use tracing::debug;

#[derive(Error, Debug)]
pub enum Error {
    #[error("The headless render device doesn't have any Argb8888 configs.")]
    NoValidConfig,
    #[error("The EGL system caused an error")]
    Egl(khronos_egl::Error),
    #[error("Error accessing operating system resources")]
    Io(io::Error),
}

impl From<khronos_egl::Error> for Error {
    fn from(value: khronos_egl::Error) -> Self {
        Error::Egl(value)
    }
}

impl From<io::Error> for Error {
    fn from(value: io::Error) -> Self {
        Error::Io(value)
    }
}

// TODO: should this be Clone? Is concurrent mutable access to raw display and context bad?
// Currently, Backend needs a copy, while a copy remains in Facade.
/// GBM EGL GPU context
#[derive(Clone, Debug)]
pub struct Egl {
    display: egl::Display,
    context: egl::Context,
    config: egl::Config,
    device: Arc<gbm::Device<File>>,
}

impl Egl {
    unsafe fn make_current(&self) {
        egl::Instance::new(egl::Static)
            .make_current(
                self.display,
                None,
                None,
                Some(self.context),
            )
            .unwrap()
    }
    unsafe fn release_current(&self) {
        egl::Instance::new(egl::Static)
            .make_current(
                self.display,
                None,
                None,
                None,
            )
            .unwrap()
    }
}


/// A shared reference to a GBM EGL GPU context.
#[derive(Clone)]
pub struct ContextRef {
    pub egl: Egl,
    /// Number of times make_current was called.
    // This counts values across threads, even though 2 make_current calls betwen threads are trouble.
    // It's because ContextRef itself will move between threads on the C side, and the data structures must survive that.
    // It's easier to be synced across threads than to reflect the restriction.
    current: Arc<AtomicUsize>,
}

impl ContextRef {
    pub fn new() -> TracedResult<Self, Error> {
        let egl_calls = egl::Instance::new(egl::Static);

        debug!("Creating an EGL context using GBM device /dev/dri/renderD128");
        
        let gbm: gbm::Device<File> = gbm::Device::new(
            // WARNING: Rust is said to always use CLOEXEC, so it's not specified here. It's still needed!
            // https://github.com/rust-lang/rust/pull/27971
            fs::OpenOptions::new()
                .read(true)
                .write(true)
                .open("/dev/dri/renderD128")
                .with_trace().map_trace_into()?
        ).with_trace().map_trace_into()?;

        egl_calls.bind_api(egl::OPENGL_API).with_trace().map_trace_into()?;

        // Older API versions don't have get_platform_display (TODO: check, this is from memory of Librem 5), so use "_ext" despite the reimplementation.
        let display: egl::Display = unsafe {
            egl_calls.get_platform_display_ext(
                ext::mesa_platform_gbm::PLATFORM_GBM_MESA,
                gbm.as_raw() as _,
                &[egl::ATTRIB_NONE],
            )
        }.with_trace().map_trace_into()?;

        egl_calls.initialize(display).with_trace().map_trace_into()?;

        // Set context
        
        // TODO: try EGL_KHR_no_config_context
        
        // get an appropriate EGL frame buffer configuration
        let configs = {
            let mut configs = Vec::with_capacity(32);
            egl_calls.choose_config(
                display, 
                &[
                    egl::BUFFER_SIZE, 32,
                    egl::DEPTH_SIZE, egl::DONT_CARE,
                    egl::STENCIL_SIZE, egl::DONT_CARE,
                    egl::RENDERABLE_TYPE, egl::OPENGL_ES2_BIT,
                    egl::SURFACE_TYPE, egl::WINDOW_BIT,
                    egl::NONE,
                ],
                &mut configs,
            ).with_trace().map_trace_into()?;
            configs
        };
        
        // Find a config whose native visual ID is the desired GBM format.
        let config = configs.into_iter().find(|config|
            egl_calls.get_config_attrib(
                display,
                *config,
                egl::NATIVE_VISUAL_ID
            ) == Ok(gbm::Format::Argb8888 as i32)
        ).ok_or(Error::NoValidConfig)?;
        
        let context = egl_calls
            .create_context(
                display,
                config,
                None,
                &[
                    egl::CONTEXT_MAJOR_VERSION, 1,
                    ext::CONTEXT_FLAGS_KHR, ext::CONTEXT_OPENGL_DEBUG_BIT_KHR,
                    egl::NONE,
                 ],
             ).with_trace().map_trace_into()?;

        Ok(Self {
            egl: Egl { context, config, device: Arc::new(gbm), display },
            current: Arc::new(AtomicUsize::new(0)),
        })
    }
    
    /// Only needed for raw::backend.
    /// Glium doesn't know that CurrentContext needs to be held in scope.
    /// Unsafe because it requires a manual release.
    pub unsafe fn force_make_current(&self) {
        if self.current.load(atomic::Ordering::SeqCst) == 0 {
            self.egl.make_current();
        }
    }

    fn release_current(&self) {
        if self.current.fetch_sub(1, atomic::Ordering::SeqCst) == 0 {
            unsafe { self.egl.release_current() };
        }
    }

    pub fn get_proc_address(procname: &str) -> *const c_void {
        egl::Instance::new(egl::Static)
            .get_proc_address(procname)
            .map(|a| a as *const c_void)
            .unwrap_or(ptr::null())
    }
    
    pub fn get_device(&self) -> Arc<gbm::Device<File>>{
        self.egl.device.clone()
    }
}

impl EglContext for ContextRef {
    fn get_display(&self) -> egl::Display {
        self.egl.display
    }

    /// Acquires the context.
    /// Relase by letting CurrentContext go out of scope.
    /// CAUTION: this will crash if multiple contexts are interleaved in a single thread. (TODO? thread-local storage could help.)
    fn make_current<'a>(&'a self) -> CurrentContext<'a> {
        if self.current.fetch_add(1, atomic::Ordering::SeqCst) == 1 {
            unsafe { self.egl.make_current() };
        }
        CurrentContext(&self)
    }
}

/// Automatically releases the context by going out of scope.
pub struct CurrentContext<'a>(&'a ContextRef);

impl<'a> Deref for CurrentContext<'a> {
    type Target = ContextRef;
    fn deref(&self) -> &Self::Target {
        self.0
    }
}

impl<'a> Drop for CurrentContext<'a> {
    fn drop(&mut self) {
        self.0.release_current();
    }
}


/// WIP: this is to find out what import_dmabuf actually needs
pub trait EglContext {
    fn make_current<'a>(&'a self) -> CurrentContext<'a>;
    fn get_display(&self) -> egl::Display;
}

/// If you intend to import a surface as a rendering target, better use ARGB8888.
/// Otherwise the config won't match.
pub fn import_dmabuf(
    egl: &impl EglContext,
    fd: DmaBuf,
    dimensions: (u32, u32),
    fourcc: gbm::Format,
) -> Result<OwnedDmabufImage, DmabufImportError> {
    let _current = egl.make_current();
    Ok(OwnedDmabufImage {
        image: unsafe {
            super::import_dmabuf_display(egl.get_display(), &fd, dimensions, fourcc)
        }?,
       dmafd: fd,
    })
}
