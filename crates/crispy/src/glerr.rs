/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! OpenGL errors.
*/

use gl;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Any")]
    Any(u32),
}

pub fn check<T>(result: T) -> Result<T, Error> {
    let err = unsafe { gl::GetError() };
    if err == gl::NO_ERROR {
        Ok(result)
    } else {
        Err(Error::Any(err))
    }
}