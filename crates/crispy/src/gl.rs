/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! OpenGL errors.
*/

use anyhow::Error;

#[derive(Error)]
pub struct Error(pub u32);