/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! OpenGL procedures.

The Shader binds the surfaces on every call.
It might be that there is performance to be gained by only doing it once
for every shader/input/output surface triple.
Or even by doing binding/import once per texture individually.
*/


pub mod egl;
pub mod egl_ext;
mod glerr;
pub mod shaders;
mod raw;
mod uniforms;

pub use cgmath;
pub use dma_boom;
pub use error_backtrace;
use egl::Dimensions;
use egl::Texture;
pub use gbm::Format;
use gl;
pub use glium;
use glium::GlObject;
use glium::texture::UnsignedTexture2d;
pub use raw::Facade;
pub use shaders::bayer;
pub use shaders::yuv;

/// Restores the OpenGL Texture2d binding in the state machine.
///
/// This won't prevent bugs: you can still call things while the wrong texture is bound. But it makes it easier to call the unbinding and hides the details.
struct Texture2DBindRestore(gl::types::GLuint);

impl Texture2DBindRestore {
    // This uses GlGet to remain compatible with GLES 2.0.
    pub fn new() -> Self {
        let mut tex_binding = 0;
        glerr::check(unsafe {
            gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut tex_binding)
        }).expect("Buggy parameter");
        Self(tex_binding as _)
    }
}

impl Drop for Texture2DBindRestore {
    fn drop(&mut self) {
        glerr::check(unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.0 as _)
        }).unwrap();
    }
}

/// Initializes the GL texture.
// For unknown reasons, passing a texture obtained this way to uniforms with the relevant filters doesn't do anything to initialize it.
// Instead, this does the necessary setup.
fn import_bind_texture<F: glium::backend::Facade>(
    facade: &F,
    tex: Texture,
) -> Result<(UnsignedTexture2d, Texture2DBindRestore), glerr::Error> {
    let Dimensions { width, height } = tex.dimensions();
    let imported = unsafe {
        use glium::texture::*;
        UnsignedTexture2d::from_id(
            facade,
            UncompressedUintFormat::U8U8U8U8,
            tex.id().as_gl_id(),
            // This unbinds the texture when dropped.
            // The ownership pertains to the ID, not the backing image
            true,
            MipmapsOption::NoMipmap,
            Dimensions::Texture2d { width, height },
        )
    };
    // Can't get Backend directly out of Facade, so we access it behind glium's back.
    gl::load_with(|s| unsafe {raw::Backend::get_proc_address(s)});
    
    let binding = Texture2DBindRestore::new();
    // Excuse me while I cargo-cult
    // whatever is happening in Texture2d::with_mipmaps.
    unsafe {
        let bind_point = gl::TEXTURE_2D;
        let id = imported.get_id();
        
        glerr::check(
            gl::BindTexture(bind_point, id)
        )?;
        glerr::check(
            gl::TexParameteri(bind_point, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_BORDER as i32)
        )?;
        glerr::check(
            gl::TexParameteri(bind_point, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32)
        )?;
        glerr::check(
            gl::TexParameteri(bind_point, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_BORDER as i32)
        )?;
        glerr::check(
            gl::TexParameteri(bind_point, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32)
        )?;
        glerr::check(
            gl::TexParameteri(bind_point, gl::TEXTURE_BASE_LEVEL, 0)
        )?;
        glerr::check(
            gl::TexParameteri(bind_point, gl::TEXTURE_MAX_LEVEL, 0)
        )?;
        // Here glium uses format RGBA8.
        // Somehow the texture is immutable. This call fails. Leaving it as commented to provide clues in case something goes wrong in the future.
        // Update: with this uncommented, the texture is blank
        //glerr::check(
            //gl::TexStorage2D(bind_point, 1, gl::R8, width as i32, height as i32)
        //)?;
    }
    Ok((imported, binding))
}