High quality Bayer demosaicing shader from the paper "[Efficient, high-quality Bayer demosaic filtering on GPUs](https://casual-effects.com/research/McGuire2009Bayer/)".

Copyright 2009 Morgan McGuire

Released under the MIT license. See [LICENSE.txt].