/*
 * SPDX-FileCopyrightText: 2009 Morgan McGuire
 *
 * SPDX-License-Identifier: MIT
 */

#version 120

attribute vec2 position; // set up by glium for compat with gl4 I guess
                
/** (w,h,1/w,1/h) */
uniform vec4            source_dims;

/** Pixel position of the first red pixel in the Bayer pattern.  [{0,1}, {0, 1}]*/
uniform vec2            first_red;

/** .xy = Pixel being sampled in the fragment shader on the range [0, 1]
    .zw = ...on the range [0, sourceSize], offset by firstRed */
varying vec4            center;

/** center.x + (-2/w, -1/w, 1/w, 2/w); These are the x-positions of the adjacent pixels.*/
varying vec4            xCoord;

/** center.y + (-2/h, -1/h, 1/h, 2/h); These are the y-positions of the adjacent pixels.*/
varying vec4            yCoord;
                    
void main() {
    // center is in [0,1) range, while position is [-1,1).
    center.xy = (position.xy + 1.0) / 2.0;
    center.zw = center.xy * source_dims.xy + first_red;
    
    vec2 invSize = source_dims.zw;
    xCoord = center.x + vec4(-2.0 * invSize.x, -invSize.x, invSize.x, 2.0 * invSize.x);
    yCoord = center.y + vec4(-2.0 * invSize.y, -invSize.y, invSize.y, 2.0 * invSize.y);

    gl_Position = vec4(position.x, position.y, 0.0, 1.0);
}