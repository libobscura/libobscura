/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

#version 120

attribute vec2 position; // set up by glium based on the Vertex struct fields

void main() {
    gl_Position = vec4(position.x, position.y, 0.0, 1.0);
}