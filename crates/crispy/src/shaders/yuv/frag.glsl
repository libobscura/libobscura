/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

#version 120

// enable for 10-bit sensors
// precision mediump float
// enable for 16-bit sensors
// precision highp float

uniform vec4 source_dims;
uniform int frameno;
uniform int color_space;
uniform int gamma;
// u8, set to GL_RED by glium
// float sampler because u8 is not supported on etnaviv
uniform sampler2D source_data;

float gamma_709(float L) {
    if (L < 0.018) {
        return L * 4.5;
    } else {
        return pow(L, 1./2.4) * 1.099 - 0.099;
    }
}

vec3 color_space_709(vec3 yuv) {
    return vec3(
        yuv.x + 1.28033 * yuv.z,
        yuv.x - 0.38059 * yuv.z - 0.21482 * yuv.y,
        yuv.x + 2.12798 * yuv.y
    );
}

vec2 flip_y(vec2 norm_coords) {
    return vec2(norm_coords.x, 1.0 - norm_coords.y);
}

float get_pixel(vec2 source_coords, float x_offset) {
    vec2 norm_coords = vec2(source_coords.x + x_offset, source_coords.y) * source_dims.zw;
    // FIXME: why is flipping y necessary for displaying on screen?
    // Is it needed for converting buffers (see conv)?
    return texture2D(source_data, flip_y(norm_coords)).r;
}

vec2 get_source_coords() {
    return vec2(floor(gl_FragCoord.x / 2.0) * 4.0 + 0.5, gl_FragCoord.y);
}

vec4 get_macropixel(vec2 source_coords) {
    return vec4(get_pixel(source_coords, 0), get_pixel(source_coords, 1),get_pixel(source_coords, 2),get_pixel(source_coords, 3));
}

void main() {
    // texelFetch arrives in GLSL 1.3
    // float g = texelFetch(bayer_data, gl_FragCoord.xy).r;
    vec2 source_coords = get_source_coords();
    if (
        source_coords.x < 0.0 || source_coords.y < 0.0
        || source_coords.x > source_dims.x || source_coords.y > source_dims.y
    ) {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        return;
    }
    vec3 yuv;
    
    // top left pixel gl_FragCoord == (0.5, 0.5)
    // and int(0.5) == 0.0 (TODO: check)
    if (int(mod(gl_FragCoord.x, 2.0)) == 0) {
        yuv = get_macropixel(source_coords).xyw;
    } else {
        yuv = get_macropixel(source_coords).zyw;
    }

    yuv.yz -= 0.5;
    
    vec3 linear;
    if (color_space == 1) {
        linear = color_space_709(yuv);
    } else {
        linear = yuv;
    }

    vec3 srgb;
    if (gamma == 1) {
        srgb = vec3(gamma_709(linear.r), gamma_709(linear.g), gamma_709(linear.b));
    } else {
        srgb = linear;
    }

    gl_FragColor = vec4(srgb, 1.0);
    //gl_FragColor = vec4(yuv.xxx, 1.0);
}