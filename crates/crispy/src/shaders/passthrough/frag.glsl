/*
 * SPDX-FileCopyrightText: 2022 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

#version 120

// enable for 10-bit sensors
// precision mediump float
// enable for 16-bit sensors
// precision highp float

uniform vec4 source_dims;
uniform int frameno;
// u8, set to GL_RED by glium
// float sampler because u8 is not supported on etnaviv
uniform sampler2D source_data;

void main() {
    // texelFetch arrives in GLSL 1.3
    // float g = texelFetch(bayer_data, gl_FragCoord.xy).r;
    vec2 coord = gl_FragCoord.xy / source_dims.xy;
    //+ (-0.5, -0.5);
    vec3 b = texture2D(source_data, coord).rgb;

    float g = 0.0;
    // gl_FragCoord is relative to frame buffer,
    // and that's the only kind of coordinate we care about
    // because the frame buffer is the debayered image.
    if (float(frameno) < gl_FragCoord.x) {
        g = 0.0;
    }
    
    gl_FragColor = vec4(b.rrr, 1.0);
}