/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! A shader outputting the same image without modifying */


use crate::egl::{Dimensions, Texture};
use glium::{program, uniform};
use std::error::Error;
use super::Shader;

pub struct Passthrough;

impl Passthrough {
    /// Creates a shader using the builtin raw EGL facade.
    pub fn new(facade: &impl glium::backend::Facade, dims: (u32, u32))
        -> Result<Shader<Self>, Box<dyn Error>>
    {
        Shader::<Self>::new_with_facade(facade, dims)
    }
}

impl Shader<Passthrough> {
    /// The only thing that differs between the native R16 and RG88 versions is the fragment shader.
    fn new_with_facade<F: glium::backend::Facade>(
        facade: &F,
        // Output dimensions
        size: (u32, u32),
    ) -> Result<Self, Box<dyn Error>> {
        let (vertices, indices) = super::covering_vertices(facade, size)?;
        Ok(Self {
            program: program!(facade,
                120 => {
                    vertex: include_str!("passthrough/vert.glsl"),
                    fragment: include_str!("passthrough/frag.glsl"),
                }
            )?,
            vertices,
            indices,
            _data: Default::default(),
        })
    }
    
    /// Draws the same image
    pub fn convert<F: glium::backend::Facade>(
        &self,
        facade: &F,
        source_tex: Texture,
        target: &mut impl glium::Surface,
    ) -> Result<(), Box<dyn Error>>{
        let Dimensions { width, height } = source_tex.dimensions();
        if (width, height) != target.get_dimensions() {
            Err(super::Error::BadDimensions{
                expected: Dimensions { width, height },
                got: source_tex.dimensions(),
                msg: "Source and target dimensions must be equal",
            })?;
        }
        self.draw_any(
            facade,
            source_tex,
            target,
            uniform! {},
        )
    }
}
