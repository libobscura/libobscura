/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! YUYV → RGBA shader for UVC webcams.
*/

use cgmath::Matrix3;
use crate::egl::{Dimensions, Texture};
use error_backtrace::{Backtraced, GenericError, Result as TraceResult};
use glium;
use glium::{program, uniform};
use std::error::Error;
use super::Shader;

/**
 * FIXME: I don't know enough about UVC, but the
 * USB Device Class Definition for Video Devices - FAQ
 * in version 1.5, 2.19 Multiple Color Matching Descriptors and table 2.6
 * states that there are 3 parameters to tweak: Color primary, transfer function, and luma matrix.
 * 
 * Not exactly sure how they work. This here is best effort. Someone with more experience can fix this.
 * 
 * So far, I've gathered:
 * 
 * - the luma transfer function gives the matrix calculating yuv = M·rgb
 * - the primaries matrix calculates rgb = P·xyz
 * - the transfer function calculates gamma
 * 
 * so the output should be srgb = gamma(M⁻¹·yuv).
 * 
 * TODO in libvidi: read the descriptor values.
 */
pub enum ColorSpace {
    /// ITU BT.470
    BT470 = 2,
    /// ITU BT.709, for HDTV
    BT709 = 1,
    /// Doesn't convert the color values
    Identity = 0,
}

impl From<ColorSpace> for Matrix3<f32> {
    fn from(value: ColorSpace) -> Self {
        match value {
            ColorSpace::BT470 => *<&Matrix3::<_>>::from(&[
                1., 0., 1.13983,
                1., -0.39465, -0.58060,
                1., 2.03211, 0.,
            ]),
            // from https://en.wikipedia.org/wiki/YUY2?useskin=vector#HDTV_with_BT.709
            ColorSpace::BT709 => *<&Matrix3::<_>>::from(&[
                1., 0., 1.28033,
                1., -0.21482, -0.38059,
                1., 2.12798, 0.,
            ]),
            ColorSpace::Identity => *<&Matrix3::<_>>::from(&[
                1., 0., 0.,
                0., 1., 0.,
                0., 0., 1.,
            ]),
        }
    }
}

pub enum Gamma {
    Srgb = 1,
    Identity = 0,
}

// TODO: add more complex filling behaviour:
// - scaling (fast/good/integer/none)
// - letterboxing direction when image doesn't fill target

/// Letterboxing behaviour.
#[derive(Debug, Default)]
pub enum Letterboxing {
    /// Moves the image to the bottom left.
    /// This is the compatible default.
    #[default]
    BottomLeft,
    /// Only accept the exact size. This is the better debugging option.
    ///
    /// If the target surface has the wrong size, the conversion will fail.
    /// Use for converting into offscreen surfaces. Onscreen surfaces may have unexpected size (when using Wayland scaling, but also when resizing).
    No,
}

/// Converts a YUV-encoded texture into a RGB one.
pub struct YuyvToRgba;

impl YuyvToRgba {
    /// Creates a shader
    pub fn new(facade: &impl glium::backend::Facade, dims: (u32, u32))
        -> Result<Shader<Self>, Box<dyn Error>>
    {
        Shader::<Self>::new_with_facade(
            facade,
            dims,
        )
    }
}

impl Shader<YuyvToRgba> {
    /// The only thing that differs between the native R16 and RG88 versions is the fragment shader.
    fn new_with_facade<F: glium::backend::Facade>(
        facade: &F,
        // Output dimensions
        size: (u32, u32),
    ) -> Result<Self, Box<dyn Error>> {
        let (vertices, indices) = super::covering_vertices(facade, size)?;
        /*use glium::CapabilitiesSource;
        dbg!(&facade.get_capabilities().supported_glsl_versions);*/
        Ok(Self {
            program: program!(
                facade,
                // The gbm backend supports this
                120 => {
                    vertex: include_str!("yuv/vert.glsl"),
                    fragment: include_str!("yuv/frag.glsl"),
                },
                // The winit backend supports this
                140 => {
                    vertex: include_str!("yuv/vert.glsl"),
                    fragment: include_str!("yuv/frag.glsl"),
                },
            )?,
            vertices,
            indices,
            _data: Default::default(),
        })
    }
    
    /// Converts image in GL texture in YUYV format `source_tex`
    /// by writing a RGBA image to GL surface `target`.
    ///
    /// TODO: the input texture should be an OpenGL texture, so that shader calls can be chained.
    /// The output is an OpenGL surface to allow rendering directly to the screen.
    pub fn convert<F: glium::backend::Facade>(
        &self,
        facade: &F,
        source_tex: Texture,
        target: &mut impl glium::Surface,
        space: ColorSpace,
        gamma: Gamma,
        letterboxing: Letterboxing,
    ) -> TraceResult<(), GenericError>{
        match letterboxing {
            Letterboxing::No => {
                let (width, height) = target.get_dimensions();
                let expected = Dimensions {
                    width: width * 2,
                    height,
                };
                
                if source_tex.dimensions().height != expected.height {
                    Err(super::Error::BadDimensions{
                        expected,
                        got: source_tex.dimensions(),
                        msg: "Source and target heights must be equal",
                    })?;
                }
                if source_tex.dimensions().width != expected.width {
                    Err(super::Error::BadDimensions{
                        expected,
                        got: source_tex.dimensions(),
                        msg: "Source width must be twice the target",
                    })?;
                }
            },
            _ => {},
        };
        // TODO: allow any monochrome format
        if source_tex.format() != gbm::Format::R8 {
            Err(super::Error::BadFormat("Only R8 supported"))?;
        }
        Ok(
            self.draw_any(
                facade,
                source_tex,
                target,
                uniform! {
                    gamma: gamma as i8,
                    color_space: space as i8,
                },
            )
            .map_err(GenericError)?
        )
    }
}
