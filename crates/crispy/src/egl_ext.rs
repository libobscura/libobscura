/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! EGL extensions compatible with Khronos-egl.
 * 
 * Currently only those used by crISPy are filled in.
*/

use khronos_egl as egl;
use std::ffi::c_void;
use std::mem;

pub const CONTEXT_FLAGS_KHR: egl::Int = 0x30FC;
pub const CONTEXT_OPENGL_DEBUG_BIT_KHR: egl::Int = 0x1;

/// Constants for [EGL_MESA_platform_gbm](https://registry.khronos.org/EGL/extensions/MESA/EGL_MESA_platform_gbm.txt)
pub mod mesa_platform_gbm {
    use khronos_egl as egl;

    pub const PLATFORM_GBM_MESA: egl::Enum = 0x31d7;
}

/// Constants for [EGL_EXT_image_dma_buf_import](https://registry.khronos.org/EGL/extensions/EXT/EGL_EXT_image_dma_buf_import.txt)
pub mod image_dma_buf_import {
    use khronos_egl as egl;

    pub const LINUX_DMA_BUF_EXT: egl::Enum = 0x3270;
    pub const LINUX_DRM_FOURCC_EXT: egl::Int = 0x3271;
    pub const DMA_BUF_PLANE0_FD_EXT: egl::Int = 0x3272;
    pub const DMA_BUF_PLANE0_OFFSET_EXT: egl::Int = 0x3273;
    pub const DMA_BUF_PLANE0_PITCH_EXT: egl::Int = 0x3274;
}

/// Implements the [EGL_EXT_platform_base extension](https://registry.khronos.org/EGL/extensions/EXT/EGL_EXT_platform_base.txt) relevant for an egl::Instance.
pub trait ExtPlatformBaseInstance {
    unsafe fn get_platform_display_ext(
        &self,
        platform: egl::Enum,
        native_display: egl::NativeDisplayType,
        attrib_list: &[egl::Attrib]
    ) -> Result<egl::Display, egl::Error>;
}

impl<T: egl::api::EGL1_4> ExtPlatformBaseInstance for egl::Instance<T> {
    unsafe fn get_platform_display_ext(
        &self,
        platform: egl::Enum,
        native_display: egl::NativeDisplayType,
        attrib_list: &[egl::Attrib]
    ) -> Result<egl::Display, egl::Error> {
        egl::check_attrib_list(attrib_list)?;
        // Calling this function every time because eglGetPlatformDisplayEXT is unlikely to be called in a speed critical context.
        let procedure = self.get_proc_address("eglGetPlatformDisplayEXT")
            .ok_or(egl::Error::BadParameter)?;
        let procedure
            : extern "system" fn(
                egl::Enum,
                egl::NativeDisplayType,
                *const usize,
            ) -> *mut c_void
            = unsafe { mem::transmute(procedure as *const c_void) };

        let ret = procedure(
            platform,
            native_display,
            attrib_list.as_ptr(),
        );
        if ret != egl::NO_DISPLAY {
            Ok(unsafe { egl::Display::from_ptr(ret) })
        } else {
            Err(self.get_error().unwrap())
        }
    }
}


/// Implements the [EGL_KHR_image_base extension](https://registry.khronos.org/EGL/extensions/KHR/EGL_KHR_image_base.txt) relevant for an egl::Instance.
pub trait KhrImageBaseInstance {
    unsafe fn create_image_khr(
        &self,
        display: egl::Display,
        ctx: egl::Context,
        target: egl::Enum,
        buffer: egl::ClientBuffer,
        attrib_list: &[egl::Int],
    ) -> Result<egl::Image, egl::Error>;
}

impl <T: egl::api::EGL1_2> KhrImageBaseInstance for egl::Instance<T> {
    unsafe fn create_image_khr(
        &self,
        display: egl::Display,
        ctx: egl::Context,
        target: egl::Enum,
        buffer: egl::ClientBuffer,
        attrib_list: &[egl::Int],
    ) -> Result<egl::Image, egl::Error> {
        egl::check_int_list(attrib_list)?;
        // Calling this function every time because eglGetPlatformDisplayEXT is unlikely to be called in a speed critical context.
        let procedure = self.get_proc_address("eglCreateImageKHR")
            .ok_or(egl::Error::BadParameter)?;
        let procedure
            : extern "system" fn(egl::NativeDisplayType, *mut c_void, egl::Enum, *mut c_void, *const egl::Int)
                -> *mut c_void
            = unsafe { mem::transmute(procedure as *const c_void) };

        unsafe {
            let image = procedure(
                display.as_ptr(),
                ctx.as_ptr(),
                target,
                buffer.as_ptr(),
                attrib_list.as_ptr(),
            );
            if image != egl::NO_IMAGE {
                Ok(egl::Image::from_ptr(image))
            } else {
                Err(self.get_error().unwrap())
            }
        }
    }
}



pub type ImageOES = *const c_void;

/// Implements the [OES_EGL_image extension](https://registry.khronos.org/OpenGL/extensions/OES/OES_EGL_image.txt) relevant for an egl::Instance.
pub trait OesImageInstance {
    unsafe fn image_target_texture_2d_oes(
        &self,
        target: egl::Enum,
        image: egl::Image,
    ) -> Result<(), egl::Error>;
}

impl <T: egl::api::EGL1_1> OesImageInstance for egl::Instance<T> {
    unsafe fn image_target_texture_2d_oes(
        &self,
        target: egl::Enum,
        image: egl::Image,
    ) -> Result<(), egl::Error> {
        // Calling this function every time because eglGetPlatformDisplayEXT is unlikely to be called in a speed critical context.
        let procedure = self.get_proc_address("glEGLImageTargetTexture2DOES")
            .ok_or(egl::Error::BadParameter)?;
        let procedure
            : unsafe extern "system" fn(egl::Enum, ImageOES) -> ()
            = unsafe { mem::transmute(procedure as *const c_void) };

        unsafe {procedure(target, image.as_ptr() )};
        match self.get_error() {
            Some(e) => Err(e),
            None => Ok(()),
        }
    }
}