/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Allow extending uniforms created using `glium::uniforms!`.
 */

use glium::uniforms::{AsUniformValue,EmptyUniforms, UniformsStorage, Uniforms};

#[macro_export]
macro_rules! uniform_extend {
    ($uniforms:expr, $($field:ident: $value:expr),+) => {
        {
            let uniforms = $uniforms;
            $(
                let uniforms = uniforms.add(stringify!($field), $value);
            )+
            uniforms
        }
    };
    ($uniforms:expr, $($field:ident: $value:expr),*,) => {
        $crate::uniform_extend!($uniforms, $($field: $value),*)
    };
}

pub trait ExtendUniforms<'n> {
    fn add<U: AsUniformValue>(self, name: &'n str, value: U)
    -> UniformsStorage<'n, U, impl Uniforms>;
}

impl<'n> ExtendUniforms<'n> for EmptyUniforms {
    fn add<U: AsUniformValue>(self, name: &'n str, value: U)
    -> UniformsStorage<'n, U, impl Uniforms> {
        UniformsStorage::new(name, value)
    }
}

impl<'n, T: AsUniformValue, U: Uniforms> ExtendUniforms<'n>
    for UniformsStorage<'n, T, U>
{
    fn add<V: AsUniformValue>(self, name: &'n str, value: V)
    -> UniformsStorage<'n, V, impl Uniforms> {
        UniformsStorage::<'n, _, _>::add(self, name, value)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use glium::uniform;
    #[test]
    fn extend_empty() {
        let u = uniform! {};
        uniform_extend!(
            u,
            test: 1u32,
            another: 1.0f32,
        );
    }
    
    #[test]
    fn extend_one() {
        let u = uniform! {
            test: 1u32,
        };
        uniform_extend!(
            u,
            test2: 1u32,
            another: 1.0f32,
        );
    }
    
    #[test]
    fn extend_many() {
        let u = uniform! {
            test: 1u32,
            aaa: -1i32,
        };
        uniform_extend!(
            u,
            test2: 1u32,
            another: 1.0f32,
        );
    }
}