media-subsystem
=======

Provides safe bindings to the [Linux Media Infrastructure API](https://docs.kernel.org/userspace-api/media/index.html)

Using
----

Add the libobscura repository to your project's `.cargo/config.toml` (or in your home directory):

```
[registries.libobscura]
index = "sparse+https://codeberg.org/api/packages/libobscura/cargo/"
```

and add the dependency to your project's `Cargo.toml`:

```
[dependencies]
media-subsystem = { version = "0.2", registry = "libobscura" }
```

Docs
---

See [libobscura README](../../README.md).

Copying
-----

See [libobscura README](../../README.md).