/* Copyright (C) 2024 DorotaC
 * Copyright (C) 2024 Fabian Thorand
 * 
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */
use std::env;
use std::fs::File;
use std::io::Read;
use logru_repl::{start, mark_err};
use tracing::info;
use tracing_subscriber;

fn main() {
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    let facts: Vec<String> = env::args_os().skip(1)
        .filter_map(|a| {
            let mut f = File::open(&a).ok()?;
            let mut data = String::new();
            f.read_to_string(&mut data).ok()?;
            info!("Loading facts from file {:?}", a);
            Some(data)
        })
        .collect();
    let facts: Vec<&str> = facts.iter().map(|s| s.as_str()).collect();
    start(
        &facts,
        |err, index| {
            let facts = facts[index];
            mark_err(facts, err)
        }
    );
}