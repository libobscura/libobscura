/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Lists available camera configurations.
*/

use clap::Parser;
use std::io;
use vidi;
use vidi::config::ConfigRequest;

/// Shows the matching formats for selected parameters.
///
/// Skipped parameters will be considered as "anything is fine".
#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Width in pixels
    #[arg(long)]
    width: Option<u32>,
    /// Height in pixels
    #[arg(long)]
    height: Option<u32>,
    /// FourCC code, e.g. YUYV or MJPG
    #[arg(long)]
    #[clap(value_parser = |s: &str| s.as_bytes().try_into().map(vidi::FourCC::new))]
    fourcc: Option<vidi::FourCC>,
    /// How many configs should be printed at most
    #[arg(long, default_value_t=32)]
    count: usize,
}

fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera = cameras_list.create(&cameras[0].info.id())
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    {
        let mut camera = camera.acquire();
        if let Ok(ref mut camera) = camera {
            let db = camera.get_supported_configs()?;
            let args = Args::parse();
            let request = ConfigRequest {
                width: args.width,
                height: args.height,
                fourcc: args.fourcc,
                ..ConfigRequest::default()
            };
            for (i, config) in db.query_configs(request).iter().enumerate() {
                if i > args.count {
                    println!("More configs available...");
                    break;
                }
                println!("Config: {:?}", config);
            }
        }
    }
    Ok(())
}