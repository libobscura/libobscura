/* Copyright (C) 2024 DorotaC
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Fetches a frame from the first camera.
*/

use std::fs;
use std::io;
use std::io::Write;
use vidi;
use vidi::CaptureStream;
use vidi::config::ConfigRequest;


fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    // Take the first camera
    let camera = cameras_list.create(&cameras[0].info.id())
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    {
        let mut camera = camera.acquire();
        if let Ok(ref mut camera) = camera {
            let configs = camera.get_supported_configs()?;
            let config = configs.query_configs(ConfigRequest {
                // Fill in desired properties here.
                // See the configs example for configurations available in your camera
                //fourcc: Some(vidi::FourCC::new(b"YUYV")),
                //width: Some(640),
                //height: Some(480),
                ..ConfigRequest::default()
            }).iter().next().expect("No supported configs");
            let mut stream = camera.start(config, 4).unwrap();
            let (buf, meta, _next) = stream.next().unwrap();
            
            let mmap = buf.memory_map_ro().unwrap();
            let data = mmap.as_slice();

            println!(
                "Buffer size: {}, seq: {}, timestamp: {}",
                data.len(),
                meta.sequence,
                meta.timestamp
            );
            fs::File::create("frame.raw").unwrap().write_all(&data).unwrap();
        }
    }
    Ok(())
}