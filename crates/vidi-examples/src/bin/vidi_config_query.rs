/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Query camera configurations with Prolog syntax.
*/

use clap::Parser;
use std::io;
use std::io::Read;
use vidi;

/** Query camera configurations with Prolog syntax.

Warning: this example is incomplete. It shows how to make the query, but not how to convert the results into Rust objects (TODO).

To find out the predicates which can be queried, the procedures and the syntax, see the `obscura_configs` tool.

The solution parser will read out the `Config` variable and turn it into a `DeviceConfig` instance if `Config` is of the correct shape. Otherwise, the raw result of the query will get printed out.

```
config_path_by_name(_, videoformat(_, _, _), Config).
```
*/
#[derive(Parser)]
#[clap(about, verbatim_doc_comment)]
struct Args {
    // this doc-comment is left-aligned and without leading stars because clap picks it up and displays it, but it can't handle the stuff on the left.
/** The Prolog-syntax query.

It must end with a period `.`, for example `config_path_by_name(_, videoformat(_, _, _), Config).`

If not present, the query will be read from standard input. Press ctrl+D to accept input.*/
    query: Option<String>,
}

fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera = cameras_list.create(&cameras[0].info.id())
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    {
        let mut camera = camera.acquire();
        if let Ok(ref mut camera) = camera {
            let db = camera.get_supported_configs()?;
            let args = Args::parse();
            let query = match args.query {
                Some(q) => q,
                None => {
                    let mut out = String::new();
                    io::stdin().read_to_string(&mut out)?;
                    out
                }
            };
            let solutions = db.query_str(&query).unwrap();
            for solution in solutions.iter() {
                if let Some(config) = solution.as_pipeline_config() {
                    println!("Config: {:?}", config);
                } else {
                    println!("Solution: {:?}", solution.as_string());
                }
            }
        }
    }
    Ok(())
}