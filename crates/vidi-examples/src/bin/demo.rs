/*
 * SPDX-FileCopyrightText: 2024 egui-glium contributors
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

use crispy::shaders::Shader;
use crispy::yuv::YuyvToRgba;
use egui::ViewportId;
use egui::widgets::{Checkbox, Slider};
use glium::glutin;
use glutin::api::egl::display::Display as EglDisplay;
use glutin::surface::WindowSurface;
use std::error::Error;
use std::io;
use std::sync::mpsc::{self, Sender};
use std::thread;
use vidi::{StreamManual, FourCC };
use vidi::config::ConfigRequest;
use vidi::v4l;
use vidi_examples::{build_egl_window, import_dmabuf_glutin};
use v4l::io::dmabuf::DmaBufProtected;
use v4l::control::Control;
use winit::{
    application::ApplicationHandler,
    event::{StartCause, WindowEvent},
    event_loop::{ActiveEventLoop, EventLoopProxy},
    window::WindowId,
};


#[derive(Debug)]
enum UserEvent {
    Controls(Vec<v4l::control::Description>),
    Values(Vec<Control>),
    Buffer,
}

enum CameraData {
    Buffer(DmaBufProtected),
}

struct UiState {
    controls: Vec<v4l::control::Description>,
    values: Vec<Control>,
}

type ControlRequest = Option<Control>;

fn main() -> io::Result<()>{
    let event_loop = winit::event_loop::EventLoop::<UserEvent>::with_user_event().build()
    .map_err(io::Error::other)?;
    
    let event_loop_proxy = event_loop.create_proxy();
    let (window, display, egl_display) = build_egl_window(&event_loop, "libobscura demo");
    
    let egui_glium = egui_glium::EguiGlium::new(ViewportId::ROOT, &display, &window, &event_loop);
    
    let shader = crispy::shaders::yuv::YuyvToRgba::new(
        &display,
        (640, 480),
    ).unwrap();
    
    let (tx, rx) = mpsc::channel();
    let (donetx, donerx) = mpsc::channel();
    
    let mut app = App {
        egui_glium,
        window,
        display,
        egl_display,
        rx,
        donetx,
        shader,
        ui_state: UiState { controls: vec![], values: vec![] },
    };
    
    start_camera(event_loop_proxy, tx, donerx).unwrap();
    
    let result = event_loop.run_app(&mut app);
    result.unwrap();
    Ok(())
}

fn start_camera(
    event_loop_proxy: EventLoopProxy<UserEvent>,
    tx: Sender<CameraData>,
    donerx: mpsc::Receiver<ControlRequest>,
) -> Result<(), Box<dyn Error>>{
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    // TODO: don't crash on zero cameras, show info instead
    let camera = cameras_list.create(&cameras[0].info.id())
        .unwrap().unwrap();
    dbg!(camera.get_id());

    // Allocate 4 buffers by default
    let buffer_count = 4;

    
    let camera = camera.acquire();
    if let Ok(mut camera) = camera {
        let configs = camera.get_supported_configs().unwrap();
        let config = configs.query_configs(ConfigRequest {
            fourcc: Some(FourCC::new(b"YUYV")),
            width: Some(640),
            height: Some(480),
            ..ConfigRequest::default()
        }).iter().next().expect("No supported configs");
        let mut controls: Vec<_> = camera.query_controls().unwrap()
            .into_iter()
            // Classes can't be queried and return Permission Denied.
            .filter(|c| c.typ != v4l::control::Type::CtrlClass)
            .collect();

        let _ = event_loop_proxy.send_event(
            UserEvent::Controls(controls.clone()),
        ).unwrap();
        thread::spawn(move || {
            let mut stream: StreamManual = camera.start_manual(
                config,
                buffer_count,
            ).unwrap();
            
            let mut buf = stream.start().unwrap();

            // The .clone() here is needed because without it, the buffer would have been moved and the `buf` name invalidated outside of the loop. Then there would be no buffer to use to finish() the capture.
            while let Ok((b, meta)) = stream.cycle_buffer(buf.clone()) {
                println!(
                    "Buffer seq: {}, timestamp: {}",
                    meta.sequence,
                    meta.timestamp,
                );
                let new_controls: Vec<_> = camera.query_controls().unwrap()
                    .into_iter()
                    // Classes can't be queried and return Permission Denied.
                    .filter(|c| c.typ != v4l::control::Type::CtrlClass)
                    .collect();
                if controls != new_controls {
                    event_loop_proxy.send_event(
                        UserEvent::Controls(new_controls.clone()),
                    ).unwrap();
                }
                controls = new_controls;
                    
                let _ = event_loop_proxy.send_event(UserEvent::Values(
                    controls.iter().map(|d| camera.control(d).unwrap()).collect()
                ));
                tx.send(CameraData::Buffer(b.clone())).unwrap();
                // Wait for the buffer to be processed before returning it.
                // If you try to return it (its clone) immediately, it will get locked and processing typically has to wait for all the in-between frames.
                // This also provides automatic frame limiting: if the processing can't keep up, buffers will be enqueued later.
                if let Some(control) = donerx.recv().unwrap() {
                    camera.set_control(control).unwrap();
                }
                buf = b;
            }

            match stream.finish(buf) {
                Ok(()) => {},
                Err((e, _buf, _stream)) => {println!("Error stopping stream: {:?}", e)},
            };
        });
    }
    Ok(())
}

struct App {
    egui_glium: egui_glium::EguiGlium,
    window: winit::window::Window,
    display: glium::Display<WindowSurface>,
    egl_display: EglDisplay,
    shader: Shader<YuyvToRgba>,
    rx: mpsc::Receiver<CameraData>,
    donetx: mpsc::Sender<ControlRequest>,
    ui_state: UiState,
}

impl UiState {    
    fn controls(&self, ui: &mut egui::Ui) -> Option<Control> {
        let mut change = None;
        ui.allocate_ui_with_layout(
            egui::Vec2 { x: ui.available_width(), y: 0. },
            egui::Layout::left_to_right(egui::Align::Min).with_main_wrap(true),
            |ui| {
                for control in &self.controls {
                    ui.allocate_ui_with_layout(
                        egui::Vec2 { x: 100., y: 0. },
                        egui::Layout::top_down(egui::Align::Center),
                        |ui| {                                  
                            let value = self.values.iter()
                                .find(|Control { id, .. }| *id == control.id)
                                .map(|Control { value, .. }| value);
                                
                            ui.horizontal(|ui| {
                                ui.label(&control.name);
                                match (value, &control.items) {
                                    (Some(v4l::control::Value::Integer(value)), None)=> {
                                        ui.with_layout(
                                            egui::Layout::right_to_left(egui::Align::Min),
                                            |ui| ui.label(value.to_string())
                                        );
                                    },
                                    _ => {},
                                }
                            });

                            let active = control.flags.intersection(v4l::control::Flags::INACTIVE).is_empty();

                            match (value, &control.items) {
                                (Some(v4l::control::Value::Integer(mut value)), None) => {
                                    if ui.add_enabled(
                                        active,
                                        Slider::new(&mut value as _, control.minimum..=control.maximum)
                                            .step_by(control.step as _)
                                            .show_value(false),
                                    ).changed() {
                                        change = Some(Control {
                                            id: control.id,
                                            value: v4l::control::Value::Integer(value),
                                        });
                                    }
                                },
                                (Some(v4l::control::Value::Integer(mut value)), Some(items)) => {
                                    let get_label = |index: i64| {
                                        items.iter()
                                            .find_map(|(v, n)| match n {
                                                v4l::control::MenuItem::Name(n) => if *v as i64 == index { Some(n.clone()) } else { None },
                                                _ => None,
                                            })
                                            .unwrap_or_else(|| index.to_string())
                                    };
                                    egui::ComboBox::from_id_salt(control.id)
                                        .selected_text(get_label(value))
                                            .show_ui(ui, |ui| {
                                                for (k, v) in items {
                                                    if ui.selectable_value(
                                                        &mut value,
                                                        *k as _,
                                                        match v {
                                                            v4l::control::MenuItem::Name(s) => s.as_str(),
                                                            _ => "?",
                                                        },
                                                    ).changed() {
                                                        change = Some(Control {
                                                            id: control.id,
                                                            value: v4l::control::Value::Integer(value),
                                                        });
                                                    }
                                                }
                                            });
                                    /*).changed() {
                                        change = Some(Control {
                                            id: control.id,
                                            value: v4l::control::Value::Integer(value),
                                        });
                                    }*/
                                },
                                (Some(v4l::control::Value::Boolean(mut value)), None) => {
                                    if ui.add_enabled(
                                        active,
                                        Checkbox::new(&mut value, "")
                                    ).changed() {
                                        change = Some(Control {
                                            id: control.id,
                                            value: v4l::control::Value::Boolean(value),
                                        });
                                    }
                                },
                                _ => {},
                            }
                        }
                    );
                }
            }
        );
        change
    }
}

impl ApplicationHandler<UserEvent> for App {
    fn resumed(&mut self, _event_loop: &ActiveEventLoop) {}

    fn user_event(&mut self, _event_loop: &ActiveEventLoop, event: UserEvent) {
        match event {
            UserEvent::Controls(controls) => { self.ui_state.controls = controls; },
            UserEvent::Values(values) => { self.ui_state.values = values },
            UserEvent::Buffer => {},
        }
    }
    
    fn window_event(&mut self, event_loop: &ActiveEventLoop, _id: WindowId, event: WindowEvent) {
        let mut redraw = || {
            let mut change = None;

            self.egui_glium.run(&self.window, |egui_ctx| {
                let my_frame = egui::containers::Frame {
                    fill: egui::Color32::from_rgba_premultiplied(0, 0, 0, 64),
                    ..Default::default()
                };
                change = egui::TopBottomPanel::bottom("bottom").frame(my_frame).show(
                    egui_ctx,
                    |ui| self.ui_state.controls(ui),
                ).inner;
            });

            {
                use glium::Surface as _;
                let mut target = self.display.draw();

                let color = egui::Rgba::from_rgb(0.1, 0.3, 1.0);
                target.clear_color(color[0], color[1], color[2], color[3]);

                let CameraData::Buffer(buf) = self.rx.recv().unwrap();

                let buf = buf.read();

                let tex = import_dmabuf_glutin(
                    &self.display,
                    &self.egl_display,
                    &*buf,
                    // The buffer has two of R8 bytes per pixel.
                    (640*2, 480),
                    // This re-interprets the texture from YUYV to R8.
                    // GPUs don't seem to like YUYV textures, and crispy shaders aren't written with anything but R8 in mind.
                    // When in doubt, check shader documentation.
                    crispy::Format::R8,
                ).unwrap();

                self.shader.convert(
                    &self.display,
                    tex.get_texture(),
                    &mut target,
                    crispy::shaders::yuv::ColorSpace::BT709,
                    crispy::shaders::yuv::Gamma::Identity,
                    crispy::yuv::Letterboxing::BottomLeft,
                ).unwrap();
    
                self.egui_glium.paint(&self.display, &mut target);
    
                target.finish().unwrap();
                
                self.donetx.send(change).unwrap();
            }
        };

        use winit::event::WindowEvent;
        match &event {
            WindowEvent::CloseRequested | WindowEvent::Destroyed => event_loop.exit(),
            WindowEvent::Resized(new_size) => {
                self.display.resize((*new_size).into());
            }
            WindowEvent::RedrawRequested => redraw(),
            _ => {}
        }

        let event_response = self.egui_glium.on_event(&self.window, &event);

        if event_response.repaint {
            self.window.request_redraw();
        }
    }

    fn new_events(&mut self, _event_loop: &ActiveEventLoop, cause: StartCause) {
        if let StartCause::ResumeTimeReached { .. } = cause {
            self.window.request_redraw();
        }
    }
}