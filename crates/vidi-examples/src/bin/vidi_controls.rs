/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Shows available controls on this device.
 * 
 * For a more comprehensive example, see query_controls in vidi-tools
*/

use std::io;
use vidi;


fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera = cameras_list.create(&cameras[0].info.id())
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    {
        let camera = camera.acquire().unwrap();
        for control in camera.query_controls().unwrap() {
            println!("{}: {}..{} every {}", control.name, control.minimum, control.maximum, control.step);
        }
    }
    Ok(())
}