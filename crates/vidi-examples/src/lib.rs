/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Common code shared by more than one example */

use crispy::dma_boom::DmaBuf;
use crispy::egl::{khronos_egl, DmabufImage};
use glium::glutin::display::AsRawDisplay;
use glium::glutin;
use glium::backend::glutin::Display;
use glium::backend::glutin::simple_window_builder::GliumEventLoop;

use winit::application::ApplicationHandler;
use winit::event::WindowEvent;
use winit::event_loop::ActiveEventLoop;
use winit::window::{Window, WindowId};


pub fn import_dmabuf_glutin(display: &Display<glutin::surface::WindowSurface>, egl_display: &glutin::api::egl::display::Display, buf: &DmaBuf, dimensions: (u32, u32), fourcc: crispy::Format) -> Result<DmabufImage, crispy::egl::DmabufImportError> {
    let raw_display = match egl_display.raw_display() {
        glutin::display::RawDisplay::Egl(d) => unsafe {khronos_egl::Display::from_ptr(d as _) },
        other => { panic!("Only EGL supported, not {:?}", other) },
    };
    unsafe {
        display.exec_in_context(|| 
            crispy::egl::import_dmabuf_display(raw_display, buf, dimensions, fourcc)
        )
    }
}

/// This exposes the EGL display instance, necessary to import the DMA-BUF into OpenGL.
pub fn build_egl_window(
    event_loop: &impl GliumEventLoop,
    title: &str,
) -> (
    Window,
    Display<glutin::surface::WindowSurface>,
    glutin::api::egl::display::Display,
) {
    use glutin::prelude::*;
    use glutin::display::GetGlDisplay;
    use glutin::config::ConfigTemplateBuilder;
    use glutin_winit::{DisplayBuilder, ApiPreference};
    use winit::raw_window_handle::HasWindowHandle;
    use std::num::NonZeroU32;

    // First we start by opening a new Window
    let display_builder =
        DisplayBuilder::new()
            .with_window_attributes(Some(
                Window::default_attributes()
                    .with_title(title)
                    .with_inner_size(winit::dpi::PhysicalSize::new(640, 480)),
            ))
            .with_preference(ApiPreference::PreferEgl);
    let (window, gl_config): (_, glutin::config::Config) = event_loop.build(
        display_builder,
        ConfigTemplateBuilder::new(),
        |configs| {
            configs
                .filter(|config| match config.display() {
                    glutin::display::Display::Egl(_) => true,
                    _ => false,
                })
                .next()
                .expect("No EGL config found")
        }
    )
    .unwrap();
    let egl_display = match gl_config.display() {
        glutin::display::Display::Egl(d) => d,
        _ => unreachable!("EGL already checked")
    };
    let window = window.unwrap();

    // Now we get the window size to use as the initial size of the Surface
    let (width, height): (u32, u32) = window.inner_size().into();
    let attrs =
        glutin::surface::SurfaceAttributesBuilder::<glutin::surface::WindowSurface>::new()
            .build(
                window.window_handle().expect("couldn't obtain raw window handle").into(),
                NonZeroU32::new(width).unwrap(),
                NonZeroU32::new(height).unwrap(),
            );

    // Finally we can create a Surface, use it to make a PossiblyCurrentContext and create the glium Display
    let surface = unsafe {
        gl_config
            .display()
            .create_window_surface(&gl_config, &attrs)
            .unwrap()
    };
    let context_attributes = glutin::context::ContextAttributesBuilder::new()
        .build(Some(window.window_handle().expect("couldn't obtain raw window handle").into()));
    let c = Some({
        let display = gl_config.display();
        unsafe { display.create_context(&gl_config, &context_attributes) }
            .expect("failed to create context")
    })
    .unwrap();

    let current_context = c
        .make_current(&surface)
        .unwrap();
    let display = Display::from_context_surface(current_context, surface).unwrap();

    (window, display, egl_display)
}

/// This structure makes it possible to write the loop handler as a closure rather than a full struct.
pub struct LoopHandler<F> {
    pub user_event: F,
}

impl<T: 'static, F: Fn(T)> ApplicationHandler<T> for LoopHandler<F> {
    fn resumed(&mut self, _event_loop: &ActiveEventLoop) {}

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        _window_id: WindowId,
        event: WindowEvent,
    ) {
        // polling and handling the events received by the window
        if let winit::event::WindowEvent::CloseRequested = event {
            event_loop.exit();
        }
    }

    fn user_event(&mut self, _event_loop: &ActiveEventLoop, event: T) {
        (self.user_event)(event)
    }
}

