[Vidi-rs](../vidi) examples
========

All examples are released under a permissive license, to let you reuse their code under any circumstances. See [COPYING.md](COPYING.md)

To run `vidi_fetch_frame.rs`:

```
cargo run --bin vidi_fetch_frame
```

[vidi_fetch_frame.rs](src/bin/vidi_fetch_frame.rs)
------

Streams a frame from a camera and stores the data to a file.

Demonstrates:
- querying cameras
- the easy buffer-borrowing API

[vidi_loop_frames.rs](src/bin/vidi_loop_frames.rs)
------

Streams frames from a camera until interrupted.

This loop works only for processing between frames, see `vidi_event_loop.rs` for integrating with an event loop.

Demonstrates:
- querying cameras
- the easy buffer-borrowing API
- where to put the a basic loop

[vidi_configs.rs](src/bin/vidi_configs.rs)
----

Shows available configs based on selected properties.

Demonstrates:
- quering available cameras
- querying a camera for available configurations

[vidi_event_loop.rs](src/bin/vidi_event_loop.rs)
------

Streams USB camera data to an OpenGL window.

This example is easy on not losing buffers due to the simple API, but sending data around is not very intuitive or fast.

Demonstrates:
- the easy buffer-borrowing API
- converting pixel formats
- passing image data to OpenGL


[vidi_shared_buffer.rs](src/bin/vidi_shared_buffer.rs)
------

Streams USB camera data to a separate thread.

This example uses the manual buffer management. This API makes buffer confusion mistakes possible, but it can be parallelized.

Demonstrates:
- manual, multithreaded buffer management
- converting pixel formats
- passing image data to OpenGL

[vidi_configs.rs](src/bin/vidi_configs.rs)
----

Lists available camera configurations.

Demonstrates:
- querying camera configurations with a basic API

[vidi_config_query.rs](src/bin/vidi_config_query.rs)
----

Queries the configuration database for available configurations.

The Prolog API lets the user specify more advanced conditions, like "use YUYV, but only when the resolution is 640×480" (TODO: demonstrate this). A more user-friendly interface is the `obscura_configs` tool in the [tools crate](../vidi-tools).

Demonstrates:
- querying camera configurations with a Prolog query

[vidi_controls.rs](src/bin/vidi_controls.rs)
----

Prints all controls on the first camera.

Demonstrates:
- querying controls

[demo.rs](src/bin/demo.rs)
----

Shows a view from the camera and all the controls.

Demonstrates:
- manual, multithreaded buffer management
- converting pixel formats
- passing image data to OpenGL
- updating controls
- reacting to controls changes
- way too much UI

This demo has a bug where it crashes with IOError. A beer to anyone who finds it [Dorota].