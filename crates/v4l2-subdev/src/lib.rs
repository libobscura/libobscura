/* Copyright (C) 2025 DorotaC
 * SPDX-License-Identifier: LGPL-2.1-or-later OR MPL-2.0
 */

/*! A safe wrapper around Linux V4l2 subdevice interface. */

use bitflags::bitflags;
use const_enum::const_enum;
use nix;
use std::mem;

/// The unsafe wrapper
#[allow(non_camel_case_types)]
#[allow(non_snake_case)]
#[allow(non_upper_case_globals)]
pub mod sys;

pub use sys::{v4l2_field, v4l2_colorspace, v4l2_quantization, v4l2_xfer_func};

pub trait Zeroed where Self: Sized {
    fn zeroed() -> Self {
        unsafe { mem::zeroed() }
    }
}

impl Zeroed for sys::v4l2_subdev_capability {}
impl Zeroed for sys::v4l2_subdev_mbus_code_enum {}
impl Zeroed for sys::v4l2_subdev_frame_size_enum {}
impl Zeroed for sys::v4l2_subdev_format {}
impl Zeroed for sys::v4l2_mbus_framefmt {}
  
const_enum! {
    #[allow(non_camel_case_types)]
    enum MediaBusFmt, sys, MEDIA_BUS_FMT_ {
        FIXED,
        RGB444_1X12,
        RGB444_2X8_PADHI_BE,
        RGB444_2X8_PADHI_LE,
        RGB555_2X8_PADHI_BE,
        RGB555_2X8_PADHI_LE,
        RGB565_1X16,
        BGR565_2X8_BE,
        BGR565_2X8_LE,
        RGB565_2X8_BE,
        RGB565_2X8_LE,
        RGB666_1X18,
        RGB666_2X9_BE,
        BGR666_1X18,
        RBG888_1X24,
        RGB666_1X24_CPADHI,
        BGR666_1X24_CPADHI,
        RGB565_1X24_CPADHI,
        RGB666_1X7X3_SPWG,
        BGR888_1X24,
        BGR888_3X8,
        GBR888_1X24,
        RGB888_1X24,
        RGB888_2X12_BE,
        RGB888_2X12_LE,
        RGB888_3X8,
        RGB888_3X8_DELTA,
        RGB888_1X7X4_SPWG,
        RGB888_1X7X4_JEIDA,
        RGB666_1X30_CPADLO,
        RGB888_1X30_CPADLO,
        ARGB8888_1X32,
        RGB888_1X32_PADHI,
        RGB101010_1X30,
        RGB666_1X36_CPADLO,
        RGB888_1X36_CPADLO,
        RGB121212_1X36,
        RGB161616_1X48,
        Y8_1X8,
        UV8_1X8,
        UYVY8_1_5X8,
        VYUY8_1_5X8,
        YUYV8_1_5X8,
        YVYU8_1_5X8,
        UYVY8_2X8,
        VYUY8_2X8,
        YUYV8_2X8,
        YVYU8_2X8,
        Y10_1X10,
        Y10_2X8_PADHI_LE,
        UYVY10_2X10,
        VYUY10_2X10,
        YUYV10_2X10,
        YVYU10_2X10,
        Y12_1X12,
        UYVY12_2X12,
        VYUY12_2X12,
        YUYV12_2X12,
        YVYU12_2X12,
        Y14_1X14,
        Y16_1X16,
        UYVY8_1X16,
        VYUY8_1X16,
        YUYV8_1X16,
        YVYU8_1X16,
        YDYUYDYV8_1X16,
        UYVY10_1X20,
        VYUY10_1X20,
        YUYV10_1X20,
        YVYU10_1X20,
        VUY8_1X24,
        YUV8_1X24,
        UYYVYY8_0_5X24,
        UYVY12_1X24,
        VYUY12_1X24,
        YUYV12_1X24,
        YVYU12_1X24,
        YUV10_1X30,
        UYYVYY10_0_5X30,
        AYUV8_1X32,
        UYYVYY12_0_5X36,
        YUV12_1X36,
        YUV16_1X48,
        UYYVYY16_0_5X48,
        SBGGR8_1X8,
        SGBRG8_1X8,
        SGRBG8_1X8,
        SRGGB8_1X8,
        SBGGR10_ALAW8_1X8,
        SGBRG10_ALAW8_1X8,
        SGRBG10_ALAW8_1X8,
        SRGGB10_ALAW8_1X8,
        SBGGR10_DPCM8_1X8,
        SGBRG10_DPCM8_1X8,
        SGRBG10_DPCM8_1X8,
        SRGGB10_DPCM8_1X8,
        SBGGR10_2X8_PADHI_BE,
        SBGGR10_2X8_PADHI_LE,
        SBGGR10_2X8_PADLO_BE,
        SBGGR10_2X8_PADLO_LE,
        SBGGR10_1X10,
        SGBRG10_1X10,
        SGRBG10_1X10,
        SRGGB10_1X10,
        SBGGR12_1X12,
        SGBRG12_1X12,
        SGRBG12_1X12,
        SRGGB12_1X12,
        SBGGR14_1X14,
        SGBRG14_1X14,
        SGRBG14_1X14,
        SRGGB14_1X14,
        SBGGR16_1X16,
        SGBRG16_1X16,
        SGRBG16_1X16,
        SRGGB16_1X16,
        JPEG_1X8,
        S5C_UYVY_JPEG_1X8,
        AHSV8888_1X32,
        METADATA_FIXED,
        META_8,
        META_10,
        META_12,
        META_14,
        META_16,
        META_20,
        META_24,
    }
}

bitflags! {
    #[derive(Debug, PartialEq, Clone, Copy)]
    pub struct MbusFrameFormatFlags: u16 {
        const SET_CSC = sys::V4L2_MBUS_FRAMEFMT_SET_CSC as u16;
    }
}

nix::ioctl_read!(querycap                       , b'V',  0, sys::v4l2_subdev_capability);
nix::ioctl_readwrite!(g_fmt                     , b'V',  4, sys::v4l2_subdev_format);
nix::ioctl_readwrite!(s_fmt                     , b'V',  5, sys::v4l2_subdev_format);
nix::ioctl_readwrite!(g_frame_interval          , b'V', 21, sys::v4l2_subdev_frame_interval);
nix::ioctl_readwrite!(s_frame_interval          , b'V', 22, sys::v4l2_subdev_frame_interval);
nix::ioctl_readwrite!(enum_mbus_code            , b'V',  2, sys::v4l2_subdev_mbus_code_enum);
nix::ioctl_readwrite!(enum_frame_size           , b'V', 74, sys::v4l2_subdev_frame_size_enum);
nix::ioctl_readwrite!(enum_frame_interval       , b'V', 75, sys::v4l2_subdev_frame_interval_enum);
nix::ioctl_readwrite!(g_crop                    , b'V', 59, sys::v4l2_subdev_crop);
nix::ioctl_readwrite!(s_crop                    , b'V', 60, sys::v4l2_subdev_crop);
nix::ioctl_readwrite!(g_selection               , b'V', 61, sys::v4l2_subdev_selection);
nix::ioctl_readwrite!(s_selection               , b'V', 62, sys::v4l2_subdev_selection);
nix::ioctl_readwrite!(g_routing                 , b'V', 38, sys::v4l2_subdev_routing);
nix::ioctl_readwrite!(s_routing                 , b'V', 39, sys::v4l2_subdev_routing);
nix::ioctl_read!(g_client_cap           , b'V',  101, sys::v4l2_subdev_client_capability);
nix::ioctl_readwrite!(s_client_cap              , b'V',  102, sys::v4l2_subdev_client_capability);
nix::ioctl_read!(g_std                  , b'V', 23, sys::v4l2_std_id);
// TODO: Not sure how this works. Not needed now.
//nix::ioctl_write_int!(s_std, b'V', 24, sys::v4l2_std_id);
nix::ioctl_readwrite!(enumstd                   , b'V', 25, sys::v4l2_standard);
nix::ioctl_readwrite!(g_edid                    , b'V', 40, sys::v4l2_edid);
nix::ioctl_readwrite!(s_edid                    , b'V', 41, sys::v4l2_edid);
nix::ioctl_read!(querystd                       , b'V', 63, sys::v4l2_std_id);
nix::ioctl_readwrite!(s_dv_timings              , b'V', 87, sys::v4l2_dv_timings);
nix::ioctl_readwrite!(g_dv_timings              , b'V', 88, sys::v4l2_dv_timings);
nix::ioctl_readwrite!(enum_dv_timings           , b'V', 98, sys::v4l2_enum_dv_timings);
nix::ioctl_read!(query_dv_timings               , b'V', 99, sys::v4l2_dv_timings);
nix::ioctl_readwrite!(dv_timings_cap            , b'V', 100, sys::v4l2_dv_timings_cap);
