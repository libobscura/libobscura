media-subsystem
=======

Provides safe bindings to the [Linux Media Infrastructure API](https://docs.kernel.org/userspace-api/media/index.html)

Using
----

Add the libobscura repository to your project's `.cargo/config.toml` (or in your home directory):

```
[registries.libobscura]
index = "sparse+https://codeberg.org/api/packages/libobscura/cargo/"
```

and add the dependency to your project's `Cargo.toml`:

```
[dependencies]
v4l2-subdev = { version = "0.1", registry = "libobscura" }
```

Docs
---

See [libobscura README](../../README.md).

Updating
---

This will automatically regenerate the bindings and promptyou for a commit message.

```
cd crates/v4l2-subdev/
sh generate.sh
```

and then commit.

Adding IOCTLs:

```
grep 'VIDIOC_' /usr/include/linux/v4l2-subdev.h | kak
```

and then type:

> %s#define VIDIOC_SUBDEV_<Return>dw`
> %s_IOWR\(<Return><Home><Home>inix::ioctl_readwrite!(<esc>
> %s_IOR\(<Return><Home><Home>inix::ioctl_read!(<esc>
> %s_IOW\(<Return><Home><Home>inix::ioctl_write!(<esc>
> <home>wwddi, <esc><end>i;
> <esc>:w ioctls.txt

This should create a copy-pasteable file with ioctls.

Copying
-----

See [libobscura README](../../README.md).