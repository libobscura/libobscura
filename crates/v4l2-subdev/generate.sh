#!/bin/sh
set -e
bindgen /usr/include/linux/v4l2-subdev.h > src/sys.rs
git add src/sys.rs
git commit -a
