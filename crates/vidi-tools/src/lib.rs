/* Copyright (C) 2025 DorotaC
SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Common functionality for command-line tools */

use std::error::Error;
use vidi::actors::camera_list;
use vidi::pipelines::UnacquiredCamera;

/// Finds a camera or prints an error and the list of available cameras.
pub fn select_camera<E>(
    cameras_list: camera_list::Tracker,
    name: Option<String>,
    on_found: impl Fn(Result<UnacquiredCamera, Box<dyn Error>>) -> Result<(), E>,
) -> Result<(), E> {
    let cameras = cameras_list.cameras();
    let camera = name.as_ref()
        .map(|name| cameras_list.create(&name));
    let show_all = || {
        println!("Available cameras:");
        for c in cameras {
            println!("  {}", c.info.id());
        }
    };
    
    match camera {
        None => show_all(),
        Some(None) => {
            show_all();
            eprintln!("No such camera: {:?}", name.unwrap());
        },
        Some(Some(camera)) => on_found(camera)?,
    }
    Ok(())
}