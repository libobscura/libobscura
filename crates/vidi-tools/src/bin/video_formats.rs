/* Copyright (C) 2024 DorotaC
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Shows video formats the output device advertises
*/

use clap::Parser;
use std::io;
use vidi;
use vidi::reexports::v4l;
use vidi::reexports::v4l2_subdev;
use v4l::capability;
use v4l::video::Capture;
use v4l2_subdev::MediaBusFmt;

/** Shows video formats the output device advertises.
The list of formats may be incomplete.

Use this to populate the output format rules in the devices file.
*/
#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Name of the camera device
    device: Option<String>,
    /// Start an interactive shell with all the facts and search library loaded
    #[arg(long, default_value_t=false)]
    repl: bool,
}


fn main() -> io::Result<()> {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera = args.device.as_ref()
        .map(|name| cameras_list.create(&name));
    let show_all = || {
        println!("Available cameras:");
        for c in cameras {
            println!("  {}", c.info.id());
        }
    };
    match camera {
        None => show_all(),
        Some(None) => {
            show_all();
            eprintln!("No such camera: {:?}", args.device.unwrap());
        }
        Some(Some(camera)) => {
            let camera = camera.expect("Failed to get camera");
            let camera = camera.acquire().expect("Failed to acquire");
            let caps = camera._video_capture_device().query_caps().unwrap();
            if caps.capabilities.intersects(capability::Flags::IO_MC) {
                for bus in MediaBusFmt::iter() {
                    for fmt in camera._video_capture_device().enum_formats_mbus_code(bus.into()).0 {
                        println!("out_format(mbus_{:?}, fourcc_{}).", bus, fmt.fourcc.to_string())
                    }
                }
            } else {
                dbg!(camera._video_capture_device().enum_formats().0);
            }
        }
    }
    Ok(())
}