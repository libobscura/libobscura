/* Copyright (C) 2025 DorotaC
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Show media device topology. */

use clap::Parser;
use logru_repl;
use std::io;
use std::path::PathBuf;
use tracing_subscriber;
use vidi;
use vidi::storage::{FileConfig, Io};
use vidi::util::media::Device;
use vidi::util::search::{topology_as_facts, TopologyDatabase};

/** Shows media device topology.

Set the environment variable `LIBOBSCURA_DEVICES_DIR` to selected directory and the configuration file will be loaded from the file `devices.pl`.
*/
#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Path to a media device (typically /dev/media*)
    device: PathBuf,
    /// Start an interactive shell with all the facts and search library loaded
    #[arg(long, default_value_t=false)]
    repl: bool,
}

fn main() -> io::Result<()> {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let device = Device::new(args.device)?;
    println!("{:#?}", device.get_topology()?);
    let topology = device.get_topology()?;
    let mut database = TopologyDatabase::new(&topology);
    let mut subdev_io = vidi::io::subdev::Io;
    database.add_subdev_info(&mut subdev_io);
    let mut config_io = FileConfig;
    for (source, facts) in config_io.device_definitions() {
        match facts {
            Ok(facts) => database.add_facts(&facts).unwrap(),
            Err(e) => println!("Failed to load device description from {:?}: {:?}", source, e),
        }
    }

    if args.repl {
        let _ = topology_as_facts(
            &device.get_topology()?,
            |f| Ok::<(), ()>(println!("{}", f)),
        );
        for fact in database.generate_subdev_facts(&mut subdev_io) {
            if !fact.is_empty() {
                println!("{}", fact);
            }
        }
        logru_repl::start_with_universe(database.into_database());
    }
    Ok(())
}