/* Copyright (C) 2025 DorotaC
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! Show media subdevice formats. */

use clap::Parser;
use media_subsystem::EntityName;
use std::io;
use std::path::PathBuf;
use tracing_subscriber;
use vidi::io::subdev;
use vidi::util::media;
use vidi::util::media::Device;

#[derive(Parser)]
#[clap(about)]
struct Args {
    /// Path to a media device (typically /dev/media*)
    device: PathBuf,
    /// Name or numeric ID of the entity to investigate
    entity: String,
}

fn main() -> io::Result<()> {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let device = Device::new(args.device)?;
    let topology = device.get_topology()?;
    let entity = topology.0.entities.iter()
        .find(|e| match &e.name {
            EntityName::Text(name) => *name == args.entity,
            _ => false,
        });
    let entity = match (args.entity.parse::<u32>(), entity) {
        (Ok(id), None) => topology.0.entities.iter()
            .find(|e| e.id.0 == id),
        (_, entity) => entity,
    };
    match entity {
        None => {
            println!("Entity not found. Available entities:");
            for e in topology.0.entities.iter() {
                println!("{:?}, {:?}", e.id, e.name);
            }
        },
        Some(entity) => {
            let interface = vidi::util::search::entity_interface(&topology, entity.id)
                .and_then(|id| topology.interface(id));
            match interface {
                None => {
                    println!("Entity {:?} has no corresponding interface", entity.name);
                },
                Some(interface) => {
                    let path = media::Io.interface_find_path(interface)?;
                    let subdev = match subdev::Io.open(&path) {
                        Err(subdev::Error::NotASubdevice) => {
                            panic!("Tried to open a path which is not a subdevice: {:?}", path);
                        },
                        Err(subdev::Error::Io(e)) => Err(e),
                        Ok(s) => Ok(s),
                    }?;
                    for pad in topology.pads(entity.id) {
                        println!("Pad index {}, role {:?}", pad.index, pad.role);
                        for code in subdev.enum_mbus_code(&mut subdev::Io, pad.index)? {
                            dbg!(&code);
                            let _ = dbg!(subdev.enum_frame_size(&mut subdev::Io, pad.index, code.code));
                        }
                        dbg!(subdev.get_format(&mut subdev::Io, pad.index)?);
                    }
                    println!("No more pads");
                },
            }
        },
    }
    Ok(())
}