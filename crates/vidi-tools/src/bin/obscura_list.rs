/*
 * SPDX-FileCopyrightText: 2023 Purism, SPC <https://puri.sm>
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! List detected cameras. */

use std::io;
use tracing_subscriber;
use vidi;

fn main() -> io::Result<()> {
    tracing_subscriber::fmt::init();
    let cameras_list = vidi::actors::camera_list::spawn().unwrap();
    let cameras = cameras_list.cameras();
    dbg!(&cameras);
    Ok(())
}