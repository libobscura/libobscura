/* Copyright (C) 2025 DorotaC
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Shows the contents of the device definition files. 
 * 
 * If your libobscura is configured correctly, you should see some device rules.
 * 
 * To see the entire contents of the database for your device, see `obscura_configs`.
*/

use clap::Parser;
use std::io;
use vidi;
use vidi::storage::{FileConfig, Io};

/// Shows the contents of the device definition files.
///
/// Use the environment variable "LIBOBSCURA_DEVICES_DIR" to set the builtin file location.
#[derive(Parser)]
#[clap(about)]
struct Args {
}

fn main() -> io::Result<()> {
    let _args = Args::parse();
    let mut config_io = FileConfig;
    for (source, facts) in config_io.device_definitions() {
        println!("Facts from {}:", source);
        println!("{:?}", facts);
        println!("-----------------");
    }
    Ok(())
}