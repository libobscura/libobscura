/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Shows available controls on this device.
*/

use std::io;
use vidi;
use vidi::reexports::v4l;
use v4l::control::MenuItem;


fn control_to_string(value: v4l::control::Value) -> String {
    match value {
        v4l::control::Value::None => "".into(),
        v4l::control::Value::Integer(i) => i.to_string(),
        v4l::control::Value::Boolean(b) => b.to_string(),
        v4l::control::Value::String(s) => s,
        other => format!("{:?}", other),
    }
}


fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera = cameras_list.create(&cameras[0].info.id())
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    {
        let camera = camera.acquire().unwrap();
        for control in camera.query_controls().unwrap() {
            match control.typ {
                v4l::control::Type::CtrlClass => println!("--- {} ---", control.name),
                _ => {
                    print!("{}: ", control.name);
                    let value = camera.control(&control).unwrap().value;
                    if let Some(ref items) = control.items {
                        print!(
                            "{:?}",
                            items.iter()
                                .map(|(_, item)| match item {
                                    MenuItem::Name(name) => String::from(name),
                                    MenuItem::Value(value) => value.to_string(),
                                })
                                .collect::<Vec<_>>()
                        );
                        let menuitem = match (&value, &control.items) {
                            (v4l::control::Value::Integer(i), Some(items))
                                => items.iter().find(|(index, _name)| *index as i64 == *i),
                            (v4l::control::Value::String(s), Some(items))
                                => items.iter().find(|(_, name)| match name {
                                    MenuItem::Name(mn) => s == mn,
                                    _ => false,
                                }),
                            _ => None,
                        };
                        let itemname = menuitem.map(|(_, item)| match item {
                            MenuItem::Name(n) => n.clone(),
                            MenuItem::Value(v) => v.to_string(),
                        });
                        println!(" ({})", itemname.unwrap_or_else(|| control_to_string(value)));
                    } else {
                        print!(
                            "{}..{}{}",
                            control.minimum,
                            control.maximum,
                            match control.step {
                                1 => "".into(),
                                other => format!(" every {}", other),
                            },
                        );
                        println!(" ({})", control_to_string(value));
                    }
                },
            }
        }
    }
    Ok(())
}