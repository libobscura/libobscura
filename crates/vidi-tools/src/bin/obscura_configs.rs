/* Copyright (C) 2025 DorotaC
SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Inspect camera configuration database.
*/

use clap::Parser;
use std::io;
use vidi;
use vidi::search;
use vidi_tools::select_camera;

/** Inspect camera configuration database.

This program prints the entire Prolog environment;
- the standard library
- the query predicated used for resolving pipelines
- the hardcoded device descriptions
- the generated device properties

if the `--repl` command line argument is present, an interactive shell (REPL) will be opened with access to those facts.

This tool is most useful when:
- working on supporting new cameras
- designing new queries

To get the pipeline configs, use the following query:

```
config_path_by_name(_, videoformat(_, _, _), Config).
```

The Prolog syntax is TODO.

The standard library allows for mathematical operations on integers (TODO: describe).

* * *

If there are no results, make sure the environment variable to the database is set:

```
LIBOBSCURA_DEVICES_DIR=crates/vidi/config cargo run --bin camera_configs
```
*/
#[derive(Parser)]
#[clap(about, verbatim_doc_comment)]
struct Args {
    /// Name of the camera device
    device: Option<String>,
    /// Start an interactive shell with all the facts and search library loaded
    #[arg(long, default_value_t=false)]
    repl: bool,
}

fn main() -> io::Result<()> {
    tracing_subscriber::fmt::init();
    let args = Args::parse();
    let cameras_list = vidi::actors::camera_list::spawn()?;
    select_camera(
        cameras_list,
        args.device,
        |camera| {
            let camera = camera.expect("Failed to get camera");
            let mut camera = camera.acquire();
            if let Ok(ref mut camera) = camera {
                let db = camera.create_configs_database::<search::SaveDumpsWrapper<search::TextualUniverse>>()?;
                for fact in db.facts() {
                    println!("{}", fact);
                }
                //let db = camera.get_supported_configs()?;
                if args.repl {
                    logru_repl::start_with_universe(db.into_database());
                }
            }
            Ok(())
        },
    )
}