const_enum
=======

Turns FFI constants into an enum, freely convertible to and from u32.
Enum variants are turned into CamelCase identifiers.
An extra variant Other(u32) exists to contain values not known to this num.
The parameters are: Enum name, path to constants, constant prefix.

```
use const_enum::const_enum;

mod sys {
    pub const NAME_B: u32 = 1;
}

const_enum! {
    enum TestEnum, sys, NAME_ {
        B,
    }
}
```

Using
----

TODO

Copying
-----

See [libobscura README](../../README.md) .