/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later
 */

/*! The const_enum macro */
#![no_std]

// The macro will get expanded within the context of the consuming crate, which may not have paste imported. This re-export allows the expanded macro to find `paste` and use it. Direct use by the consuming crate is discouraged. This re-export should be treated as an implementation detail.
#[doc(hidden)]
pub use paste::paste;

/// Turns FFI constants into an enum, freely convertible to and from u32.
/// Enum variants are turned into CamelCase identifiers.
/// An extra variant Other(u32) exists to contain values not known to this num.
/// The parameters are: Enum name, path to constants, constant prefix.
///
/// ```
/// use const_enum::const_enum_camel;
/// mod sys {
///     pub const NAME_B: u32 = 1;
/// }
/// 
/// const_enum_camel! {
/// // you can also use "this" as the path if the constants are in the same scope. It doesn't work in doctests, though, so I added the "mod sys".
///     enum TestEnum, sys, NAME_ {
///         B,
///     }
/// }
/// ```
#[macro_export]
macro_rules! const_enum_camel {
    {
        $( #[$enummeta:meta] )*
        enum $name:ident, $path:path, $prefix:ident {
        $(
            $( #[$meta:meta] )*
            $c:ident
        ),* $(,)?
    }} => {
        $crate::paste! {
            $( #[$enummeta] )*
            #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
            #[repr(u32)]
            pub enum $name {
                $(
                    $( #[$meta] )*
                    [<$c:camel>],
                )*
                /// Any value. Methods on this type will always prefer to create one of the above variants. Values covered by them can only be created by instantiating this variant explicitly. 
                Other(u32),
            }
        }
        impl From<u32> for $name {
            fn from(v: u32) -> Self {
                match v {
                    $(
                        $crate::paste! { $path::[<$prefix$c>] }
                        => $crate::paste! { Self::[<$c:camel>] },
                    )*
                    other => Self::Other(other),
                }
            }
        }
        impl From<$name> for u32 {
            fn from(v: $name) -> Self {
                match v {
                    $(
                        $crate::paste! { $name::[<$c:camel>] } => 
                        $crate::paste! { $path::[<$prefix$c>] },
                    )*
                    $name::Other(v) => v,
                }
            }
        }
    };
}

/// Turns FFI constants into an enum, freely convertible to and from u32.
/// An extra variant Other(u32) exists to contain values not known to this num.
/// The parameters are: Enum name, path to constants, constant prefix.
///
/// ```
/// use const_enum::const_enum;
/// mod sys {
///     pub const NAME_B: u32 = 1;
/// }
/// 
/// const_enum! {
/// // you can also use "this" as the path if the constants are in the same scope. It doesn't work in doctests, though, so I added the "mod sys".
///     enum TestEnum, sys, NAME_ {
///         B,
///     }
/// }
/// ```
#[macro_export]
macro_rules! const_enum {
    {
        $( #[$enummeta:meta] )*
        enum $name:ident, $path:path, $prefix:ident {
        $(
            $( #[$meta:meta] )*
            $c:ident
        ),* $(,)?
    }} => {
        $crate::paste! {
            $( #[$enummeta] )*
            #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
            #[repr(u32)]
            pub enum $name {
                $(
                    $( #[$meta] )*
                    [<$c>],
                )*
                /// Any value. Methods on this type will always prefer to create one of the above variants. Values covered by them can only be created by instantiating this variant explicitly. 
                Other(u32),
            }
        }
        impl From<u32> for $name {
            fn from(v: u32) -> Self {
                match v {
                    $(
                        $crate::paste! { $path::[<$prefix$c>] }
                        => $crate::paste! { Self::[<$c>] },
                    )*
                    other => Self::Other(other),
                }
            }
        }
        impl From<$name> for u32 {
            fn from(v: $name) -> Self {
                match v {
                    $(
                        $crate::paste! { $name::[<$c>] } => 
                        $crate::paste! { $path::[<$prefix$c>] },
                    )*
                    $name::Other(v) => v,
                }
            }
        }
        impl $name {
            /// Iterates all known valid values
            pub fn iter() -> impl Iterator<Item=Self> {
                [$(
                    $crate::paste! { Self::[<$c>] },
                )*].into_iter()
            }
        }
    };
}

#[cfg(test)]
mod test {
    const NAME_B: u32 = 1;
    
    const_enum! {
        enum TestEnum, self, NAME_ {
            B,
        }
    }

    #[test]
    fn test_from() {
        assert_eq!(TestEnum::from(1), TestEnum::B);
        assert_eq!(TestEnum::from(2), TestEnum::Other(2));
    }
    #[test]
    fn test_to() {
        assert_eq!(u32::from(TestEnum::B), 1);
        assert_eq!(u32::from(TestEnum::Other(2)), 2);
    }
    #[test]
    fn test_bad_other() {
        // It's undesired but allowed to construct an Other with a value that better matches another enum variant
        assert_eq!(u32::from(TestEnum::B), u32::from(TestEnum::Other(1)));
    }
}