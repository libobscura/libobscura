# Tests for libvidi

Those tests require the presence of a camera, so they shouldn't be executed when `cargo test` runs. They are also not examples or main binaries, so they live in a separate crate to avoid confusion.