/* Copyright (C) 2024 DorotaC
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Tests that a config can be reused betwen instances of the same camera.
 * 
 * Requires a camera to be present.
*/

use std::io;
use vidi;
use vidi::config::ConfigRequest;

fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera_id = &cameras[0].info.id();
    let camera = cameras_list.create(camera_id)
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    let config = {
        let mut camera = camera.acquire().unwrap();
        let db = camera.get_supported_configs()?;
        let conf = db.query_configs(ConfigRequest::default()).iter().next().unwrap();
        conf
    };
    {
        let cameras_list = vidi::actors::camera_list::spawn()?;
        let camera = cameras_list.create(camera_id)
            .expect("No such camera")
            .expect("Failed to get camera");
        let mut camera = camera.acquire().unwrap();
        assert!(camera.start(config, 4).is_ok())
    }
    Ok(())
}