/*
 * SPDX-FileCopyrightText: 2024 DorotaC
 *
 * SPDX-License-Identifier: MIT OR Apache-2.0
 */

/*! Tests that a config can not be reused betwen different cameras.
 * 
 * Requires two cameras to be present.
*/

use std::io;
use vidi;
use vidi::config::ConfigRequest;

fn main() -> io::Result<()> {
    let cameras_list = vidi::actors::camera_list::spawn()?;
    let cameras = cameras_list.cameras();
    let camera_id = &cameras.get(0).expect("No camera found").info.id();
    let other_camera_id = &cameras.get(1).expect("Another camera needed").info.id();
    let camera = cameras_list.create(camera_id)
        .expect("No such camera")
        .expect("Failed to get camera");
    dbg!(camera.get_id());
    let config = {
        let mut camera = camera.acquire().unwrap();
        let db = camera.get_supported_configs()?;
        let conf = db.query_configs(ConfigRequest::default()).iter().next().unwrap();
        conf
    };
    {
        let camera = cameras_list.create(other_camera_id)
            .expect("No such camera")
            .expect("Failed to get camera");
        dbg!(camera.get_id());
        let mut camera = camera.acquire().unwrap();
        assert!(camera.start(config, 4).is_err())
    }
    Ok(())
}