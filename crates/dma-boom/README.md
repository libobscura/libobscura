# DMA-Boom

Handles Linux [DMA-BUF](https://docs.kernel.org/userspace-api/dma-buf-alloc-exchange.html)fers without exploding (safely).

The DMA-Buf mechanism in Linux is aimed at providing a way for the user-space to efficiently
share memory buffers between multiple devices, without any copy.

This library provides a safe abstraction over this interface for Rust.

## Basic usage

```should_panic()

use dma_boom::DmaBuf;
use dma_boom::test;

// This test uses a stub device, always panicking.
// It's up to you to find a working buffer.
let buf: &DmaBuf = test::get_dma_buf();

{
    // Request sync and create an access guard.
    // Multiple read-only accesses can co-exist
    let mmap = buf.memory_map_ro().unwrap();
    // The actual slice
    let data = mmap.as_slice();
    if data.len() >= 4 {
        println!("Data buffer: {:?}...", &data[..4]);
    }
} // `mmap` goes out of scope and unmaps the buffer

let buf: &mut DmaBuf = test::get_dma_buf_mut();

{
    // Write access is only allowed for mutable borrows
    let mut mmap_rw = buf.memory_map_rw().unwrap();
    let data = mmap_rw.as_slice_mut();
    if data.len() >= 4 {
        data[0] = 0;
        println!("Data buffer: {:?}...", &data[..4]);
    }
}
```