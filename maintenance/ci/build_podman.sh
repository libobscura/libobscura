#!/bin/sh

# SPDX-FileCopyrightText: 2024 DorotaC
#
# SPDX-License-Identifier: MPL-2.0 OR LGPL-2.1-or-later

set -e
DIRPATH="`pwd`/../.."
FULLPATH=`realpath "${DIRPATH}"`
podman image untag fedora:latest
podman build --volume "${FULLPATH}":/sources:rw --file ./fedora.Dockerfile --tag fedora-libobscura