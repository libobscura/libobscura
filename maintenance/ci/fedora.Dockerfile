# Pre-install dependencies to reduce load on the server
FROM fedora:latest

RUN dnf -y upgrade

RUN dnf install -y rust cargo

RUN dnf install -y git

RUN dnf install -y libudev-devel clang-devel libglvnd-devel libgbm-devel

RUN cargo install mdbook

RUN cd /sources && cargo fetch