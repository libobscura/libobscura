The runner_config.yml file contains the Forgejo runner configuration, modified to enable access to the necessary hardware resources.

The build_podman.sh file builds a Podman image suitable for use on the runner machine.